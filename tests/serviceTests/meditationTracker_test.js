describe('meditationTracker', function () {
  var meditationTracker,
      practiceStatsManager,
      $rootScope;

  beforeEach(function () {
    module('workwellnw.services', function ($provide) {
      $provide.factory('practiceStatsManager', function () {
        var service = {};
        service.addOne = jasmine.createSpy('practiceStatsManager.addOne').and.stub();
        service.flush = jasmine.createSpy('practiceStatsManager.flush').and.stub();
        return service;
      })
    });

    inject(function (_meditationTracker_, _practiceStatsManager_, _$rootScope_) {
      meditationTracker = _meditationTracker_;
      practiceStatsManager = _practiceStatsManager_;
      $rootScope = _$rootScope_;
    })
  })

  it('should call Date.now', function () {
    var dateVal;
    spyOn(Date, 'now').and.callFake(function () {
      var current = 0;
      return function () {
        current += 5000;
        dateVal = current;
        return current;
      };
    }());

    meditationTracker();
    $rootScope.$emit('meditation:start');
    $rootScope.$apply();
    expect(Date.now).toHaveBeenCalled();
    expect(dateVal).toBe(5000);
    $rootScope.$emit('meditation:break', true);
    expect(Date.now).toHaveBeenCalled();
    expect(practiceStatsManager.addOne).toHaveBeenCalledWith(5000, true);
    expect(dateVal).toBe(10000);
  })

  it("should flush practiceStatsManager when it's done", function () {
    meditationTracker();
    $rootScope.$emit('meditation:done');
    expect(practiceStatsManager.flush).toHaveBeenCalled();
  })

  it("should do nothing on break if start wasn't called first", function () {
    meditationTracker();
    $rootScope.$emit('meditation:break');
    expect(practiceStatsManager.addOne).not.toHaveBeenCalled();
  })
})
