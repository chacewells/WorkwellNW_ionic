describe('mindfulMinuteManager', function () {
    describe('update', function () {
        var mindfulMinuteManager,
            mindfulMinutePersistence,
            mindfulMinuteScheduler,
            mindfulMinutes;

        beforeEach(function () {
            mindfulMinutes = [];
            module('workwellnw.services', ionicPlatformProvider);
            inject(function (_mindfulMinuteManager_, _mindfulMinutePersistence_, _mindfulMinuteScheduler_) {
                mindfulMinuteManager = _mindfulMinuteManager_;
                mindfulMinutePersistence = _mindfulMinutePersistence_;
                mindfulMinuteScheduler = _mindfulMinuteScheduler_;
            })
        })

        it('should call mindfulMinutePersistence.save and mindfulMinuteScheduler.schedule with mindfulMinutes', function () {
            var one = new Date(2015, 6, 10, 9, 30, 0, 0),
                two = new Date(2015, 6, 10, 10, 15, 0, 0),
                actualSave,
                actualSchedule;

            mindfulMinutes.push({
                timeOfDay: one
            });
            mindfulMinutes.push({
                timeOfDay: two
            });
            spyOn(mindfulMinutePersistence, 'save').and.callFake(function (mindfulMinutes) {
                actualSave = mindfulMinutes;
            });
            spyOn(mindfulMinuteScheduler, 'schedule').and.callFake(function (mindfulMinutes) {
                actualSchedule = mindfulMinutes;
            });

            mindfulMinuteManager.update(mindfulMinutes);
            expect(actualSave).toEqual(actualSchedule);
            expect(actualSave[0].timeOfDay).toEqual(one);
            expect(actualSave[1].timeOfDay).toEqual(two);
        })
    })
})
