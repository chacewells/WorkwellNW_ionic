describe('persistenceManager', function () {
    var persistenceManager,
        storedValue,
        storedObj;

    beforeEach(function () {
        storedValue = '{"key": "value"}';
        storedObj = {
            key: 'value'
        };

        module('workwellnw.services', function ($provide) {
            $provide.factory('$window', function () {
                var service = {};
                service.localStorage = {};
                service.localStorage.setItem = spyOn(localStorage, 'setItem').and.callFake(function () {});
                service.localStorage.getItem = spyOn(localStorage, 'getItem').and.callFake(function (key) {
                    switch (key) {
                    case 'key':
                        return storedValue;
                    case 'nullKey':
                        return null;
                    case 'emptyStringKey':
                        return '';
                    case 'undefinedKey':
                        return;
                    case 'zeroKey':
                        return 0;
                    default:
                        return null;
                    }
                });
                service.localStorage.clear = spyOn(localStorage, 'clear').and.callFake(function () {});
                return service;
            });
        });

        inject(function (_$window_, _persistenceManager_) {
            $window = _$window_;
            persistenceManager = _persistenceManager_;
        });
    })

    it('should persist when i say persist', function () {
        expect($window.localStorage.setItem).not.toHaveBeenCalled();
        var obj = {
            hello: 'world'
        };
        var objJSON = JSON.stringify(obj);
        persistenceManager.persist('key', obj);
        expect($window.localStorage.setItem).toHaveBeenCalledWith('key', objJSON);
    });

    it('should retrieve some value', function () {
        expect($window.localStorage.getItem).not.toHaveBeenCalled();
        var obj = persistenceManager.retrieve('key');
        expect(obj).toEqual(storedObj);
        expect($window.localStorage.getItem).toHaveBeenCalledWith('key');
    });

    it('should return null when localStorage returns empty stuff', function () {
        var obj = persistenceManager.retrieve('nullKey');
        expect(obj).toBe(null);

        obj = 'hello';
        obj = persistenceManager.retrieve('undefinedKey');
        expect(obj).toBe(null);

        obj = 'hello';
        obj = persistenceManager.retrieve('emptyStringKey');
        expect(obj).toBe(null);

        obj = 'hello';
        obj = persistenceManager.retrieve('zeroKey');
        expect(obj).toBe(null);
    });

    it('should clear when i say clear', function () {
        expect($window.localStorage.clear).not.toHaveBeenCalled();
        persistenceManager.clear();
        expect($window.localStorage.clear).toHaveBeenCalled();
    });
});