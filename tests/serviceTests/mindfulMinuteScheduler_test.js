describe('mindfulMinuteScheduler', function () {
    var mindfulMinuteTemplates,
        mindfulMinuteUtil,
        $cordovaLocalNotification,
        mindfulMinuteScheduler,
        scheduledDate,
        dateUtil;

    beforeEach(function () {
        module('workwellnw.services', ionicPlatformProvider);

        inject(function (_mindfulMinuteTemplates_, _mindfulMinuteUtil_, _$cordovaLocalNotification_, _mindfulMinuteScheduler_, _dateUtil_) {
            mindfulMinuteUtil = _mindfulMinuteUtil_;
            mindfulMinuteTemplates = _mindfulMinuteTemplates_;
            $cordovaLocalNotification = _$cordovaLocalNotification_;
            mindfulMinuteScheduler = _mindfulMinuteScheduler_;
            dateUtil = _dateUtil_;
            spyOn(mindfulMinuteTemplates, 'random').and.callFake(function () {
                return {
                    title: "Mindful Minute",
                    message: "This doesn't matter",
                    sound: "Doesn't matter"
                };
            });
        })
    })

    it('should have defined dependencies', function () {
        expect(mindfulMinuteTemplates).toBeDefined();
        expect($cordovaLocalNotification).toBeDefined();
        expect(mindfulMinuteScheduler).toBeDefined();
    })

    describe('toWeekSeparatedInstances', function () {
        var mindfulMinutes,
            dateUtil;

        beforeEach(function () {
            mindfulMinutes = [];
        })

        it('should create week-separated mindful-minute instances of each mindful minute in the array', function () {
            var one = new Date(2015, 1, 15, 9, 30, 0, 0),
                two = new Date(2015, 1, 15, 10, 15, 0, 0);

            mindfulMinutes.push({
                timeOfDay: one
            });
            mindfulMinutes.push({
                timeOfDay: two
            });

            var actual = mindfulMinuteScheduler.toWeekSeparatedInstances(mindfulMinutes, 3);
            expect(actual[0].at).toEqual(one);
            expect(actual[1].at).toEqual(new Date(2015, 1, 22, 9, 30, 0, 0));
            expect(actual[2].at).toEqual(new Date(2015, 1, 29, 9, 30, 0, 0));

            expect(actual[3].at).toEqual(two);
            expect(actual[4].at).toEqual(new Date(2015, 1, 22, 10, 15, 0, 0));
            expect(actual[5].at).toEqual(new Date(2015, 1, 29, 10, 15, 0, 0));
        })
    })

    describe('scheduleOut', function () {
        it('should shedule each mindful minute to consecutive weeks for configured notification limit', function () {
            var actualPreparedArray;
            var one = new Date(2015, 1, 15, 9, 30, 0, 0),
                two = new Date(2015, 1, 15, 10, 15, 0, 0),
                nowDate = new Date(2015, 6, 13, 10, 0, 0, 0);

            spyOn(dateUtil, 'now').and.callFake(function () {
                return angular.copy(nowDate);
            });

            spyOn($cordovaLocalNotification, 'schedule').and.callFake(function (prepared) {
                actualPreparedArray = prepared;
            });

            mindfulMinuteScheduler.scheduleOut([
                {
                    timeOfDay: one
                },
                {
                    timeOfDay: two
                }
            ]);

            expect(actualPreparedArray.length).toBe(62);
            expect(actualPreparedArray[0].at).toEqual(new Date(2015, 6, 14, 9, 30, 0, 0));
            expect(actualPreparedArray[1].at).toEqual(new Date(2015, 6, 21, 9, 30, 0, 0));
            expect(actualPreparedArray[31].at).toEqual(new Date(2015, 6, 13, 10, 15, 0, 0));
            expect(actualPreparedArray[32].at).toEqual(new Date(2015, 6, 20, 10, 15, 0, 0));
            expect(actualPreparedArray[1].id).toBe(1); //sanity check
            expect(actualPreparedArray[61].id).toBe(61); //another sanity check
        })
    })

})
