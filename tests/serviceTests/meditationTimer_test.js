describe('meditationTimer', function () {
  var meditationTimer,
  $cordovaNativeAudio,
  $timeout,
  $interval;
  beforeEach(function () {
    module('workwellnw.services', ionicPlatformProvider, function ($provide) {
      $provide.factory('$cordovaNativeAudio', function ($q) {
        var service = {};
        service.preloadSimple = jasmine.createSpy('preloadSimple').and.callFake(function (sound) {
          var deferred = $q.defer();
          return deferred.promise;
        });
        service.play = jasmine.createSpy('play').and.callFake(Function());
        return service;
      });
    });
    inject(function (_meditationTimer_, _$cordovaNativeAudio_, _$timeout_, _$interval_) {
      meditationTimer = _meditationTimer_;
      $cordovaNativeAudio = _$cordovaNativeAudio_;
      $timeout = _$timeout_;
      $interval = _$interval_;
    })
  })

  it('should tick when i say tick', function () {
    var timer = meditationTimer.withSound('sound');
    expect($cordovaNativeAudio.preloadSimple).toHaveBeenCalledWith('sound_playback_id', 'media/sound.mp3');
    var cb = jasmine.createSpy('cb').and.callFake(Function());
    timer.setTime({minutes: 5, seconds: 1});
    timer.start(cb);
    expect(timer.cb).toBe(cb);
    expect(cb).toHaveBeenCalledWith({hours: 0, minutes: 5, seconds: 1});
    $timeout.flush(1000);
    expect(cb).toHaveBeenCalledWith({hours:0,minutes:5,seconds:0});
    expect($cordovaNativeAudio.play).toHaveBeenCalled();
  })

  it("should call $interval and play the sound 3 times when it's done", function () {
    var timer = meditationTimer.withSound('sound').setTime({seconds: 2});
    var cb = jasmine.createSpy('cb').and.callFake(Function());
    timer.start(cb);
    expect(cb.calls.count()).toBe(1);
    expect(cb).toHaveBeenCalledWith({hours:0,minutes:0,seconds:2});
    $timeout.flush(1000);
    expect(cb.calls.count()).toBe(2);
    expect(cb).toHaveBeenCalledWith({hours:0,minutes:0,seconds:1});
    $timeout.flush(1000);
    expect(cb).toHaveBeenCalledWith({hours:0,minutes:0,seconds:0});
    expect($cordovaNativeAudio.play).toHaveBeenCalled();
    expect($cordovaNativeAudio.play.calls.count()).toBe(1);
    $interval.flush(1000);
    expect($cordovaNativeAudio.play.calls.count()).toBe(2);
    $interval.flush(1000);
    expect($cordovaNativeAudio.play.calls.count()).toBe(3);
    $interval.flush(1000);
    expect($cordovaNativeAudio.play.calls.count()).toBe(3);
  })

  it("should be running (boolean) when it's running", function () {
    var timer = meditationTimer.withSound('sound').setTime({minutes: 1, seconds: 2});
    var cb = jasmine.createSpy('cb').and.callFake(Function());
    timer.start(cb);
    expect(timer.running).toEqual(true);
    timer.stop();
    expect(timer.running).toEqual(false);
  })

  it("should not be done until it's done running", function () {
    var timer = meditationTimer.withSound('sound').setTime({minutes: 1});
    var cb = jasmine.createSpy('cb').and.callFake(Function());
    expect(timer.done).toEqual(true);
    timer.start(cb);
    expect(timer.done).toEqual(false);
    timer.pause();
    expect(timer.done).toEqual(false);
    timer.resume();
    expect(timer.done).toEqual(false);
    timer.stop();
    expect(timer.done).toEqual(true);
  })
})
