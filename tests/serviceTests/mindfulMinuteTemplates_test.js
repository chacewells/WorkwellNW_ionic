describe('mindfulMinuteTemplates', function () {
    //  TODO: this test will break at some point in the future, when messages are populated with persistent values
    var mindfulMinuteTemplates,
        $rootScope,
        $log,
        jsonResourceManager,
        $httpBackend;

    beforeEach(function () {
        module('workwellnw.services', ionicPlatformProvider);
        inject(function (_mindfulMinuteTemplates_, _jsonResourceManager_, _$rootScope_, _$log_, _$httpBackend_) {
            mindfulMinuteTemplates = _mindfulMinuteTemplates_;
            jsonResourceManager = _jsonResourceManager_;
            $rootScope = _$rootScope_;
            $log = _$log_;
            $httpBackend = _$httpBackend_;
        });
    });

    it('should return some object with properties "title", "message", and "sound"', function () {
        spyOn(Math, 'random').and.returnValue(0);
        $httpBackend.when('GET', 'data/templates.json').respond(200, ['There is time between peace and tranquility.']);
        $httpBackend.flush();
        var template = mindfulMinuteTemplates.random();
        expect(Math.random).toHaveBeenCalled();
        expect(template.message).toBe('There is time between peace and tranquility.');
    });
});
