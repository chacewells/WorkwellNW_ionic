describe('mediaUrl', function () {
  var mediaUrl,
      $ionicPlatform,
      $rootScope;

  beforeEach(function () {
    module('workwellnw.services', function ($provide) {
      $provide.factory('$ionicPlatform', ['$q', function ($q) {
        var service = {};
        service.ready = jasmine.createSpy('ready').and.callFake(function (cb) {
          var deferred = $q.defer();
          cb && cb();
          return deferred.promise;
        });
        return service;
      }])
    });
    inject(function (_$rootScope_, _mediaUrl_) {
      $rootScope = _$rootScope_;
      mediaUrl = _mediaUrl_;
    });
  })

  it('should have a reference to cordova.file.applicationDirectory', function () {
    expect(cordova).toBeDefined();
    expect(cordova.file).toBeDefined();
  })

  it('should return a valid url for "star.mp3" resource', function () {
    $rootScope.$apply();
    var star_mp3_url = mediaUrl.mp3('star');
    expect(star_mp3_url).toEqual('file://media/star.mp3');
  })

})
