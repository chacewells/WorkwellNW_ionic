function FileEntry(path) {
  this.path = path;
}

var cordova = {
  file: {
    applicationDirectory: 'applicationDirectory'
  }
};

var md5sum = {};

describe('audioDownload', function () {
  var audioDownload,
    $cordovaFile,
    $cordovaFileTransfer,
    $q;

  beforeEach(function () {
    module('workwellnw.services');

    md5sum.file = jasmine.createSpy('md5sum.file')
    .and.callFake(Function());
    inject(function (_audioDownload_, _$cordovaFileTransfer_, _$q_) {
      audioDownload = _audioDownload_;
      $cordovaFile = cordova.file;
      $cordovaFileTransfer = _$cordovaFileTransfer_;
      $q = _$q_;
    });
  });

  it('should not fail', function () {
    expect(1).toBe(1);
    expect($cordovaFile).toBeDefined();
    expect($cordovaFileTransfer).toBeDefined();
  });

  it("Should request from the expected URL", function () {
    spyOn($cordovaFileTransfer, 'download').and.callFake(function (url, tPath, opts, trustHosts) {
      return $q(Function(), Function());
    });
    var host = 'www.frenchy.com', basePath = 'this/that';
    var dl = audioDownload('http://' + host + '/' + basePath);
    dl.downloadFile('file.mp3', $cordovaFile.applicationDirectory + '/www/media/file.mp3');
    expect($cordovaFileTransfer.download)
    .toHaveBeenCalledWith(
      'http://' + host + '/' + basePath + '/file.mp3',
      cordova.file.applicationDirectory + '/www/media/file.mp3',
      jasmine.any(Object),
      jasmine.any(Boolean)
    );
  });

  it("Should check the md5sum", function () {
    spyOn($cordovaFileTransfer, 'download').and.callFake(function () {
      return $q(Function(), Function());
    });

    var host = 'www.dont.care',
        basePath = 'dont/care',
        filename = 'dontcare.mp3',
        localFile = $cordovaFile.applicationDirectory + '/i/do/care.mp3';
    audioDownload(host, basePath)
      .downloadFile('file.mp3', localFile);
  });
});
