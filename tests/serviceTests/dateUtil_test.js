describe('dateUtil', function () {
    var dateUtil;

    beforeEach(function () {
        module('workwellnw.services');
        inject(function (_dateUtil_) {
            dateUtil = _dateUtil_;
        })
    })

    describe('nextTimeOfDay()', function () {
        it("should return a date with the same day as now given a date with a time of day after now's", function () {
            var now = new Date(2000, 2, 1);
            now.setHours(12);
            now.setMinutes(30);
            now.setSeconds(0, 0);
            var timeOfDay = new Date(1970, 1, 1);
            timeOfDay.setHours(12 + 1);
            timeOfDay.setMinutes(4);
            timeOfDay.setSeconds(0, 0);

            var afterNow = dateUtil.nextTimeOfDay(timeOfDay, now);
            expect(afterNow.getHours()).toBe(12 + 1);
            expect(afterNow.getMinutes()).toBe(4);
            expect(afterNow.getSeconds()).toBe(0);
            expect(afterNow.getMonth()).toBe(2);
            expect(afterNow.getDate()).toBe(1);
        })

        it('should return a date with the next day from now given a date with a time of day before now', function () {
            var now = new Date(2000, 3, 14);
            now.setHours(9);
            now.setMinutes(54);
            now.setSeconds(0, 0);
            var timeOfDay = new Date(1970, 1, 1);
            timeOfDay.setHours(9);
            timeOfDay.setMinutes(53);
            timeOfDay.setSeconds(59, 0);

            var afterNow = dateUtil.nextTimeOfDay(timeOfDay, now);
            expect(afterNow.getHours()).toBe(9);
            expect(afterNow.getMinutes()).toBe(53);
            expect(afterNow.getSeconds()).toBe(59);
            expect(afterNow.getMonth()).toBe(3);
            expect(afterNow.getDate()).toBe(15);
        })

        it('should return a date with the next day from now given a date with a time of day the same as now', function () {
            var now = new Date(2000, 6, 11);
            now.setHours(12 + 8);
            now.setMinutes(48);
            now.setSeconds(21, 0);
            var timeOfDay = new Date(1970, 3, 25);
            timeOfDay.setHours(now.getHours());
            timeOfDay.setMinutes(now.getMinutes());
            timeOfDay.setSeconds(now.getSeconds(), 0);

            var afterNow = dateUtil.nextTimeOfDay(timeOfDay, now);
            expect(afterNow.getHours()).toBe(12 + 8);
            expect(afterNow.getMinutes()).toBe(48);
            expect(afterNow.getSeconds()).toBe(21);
            expect(afterNow.getMonth()).toBe(6);
            expect(afterNow.getDate()).toBe(12);
        })

        describe('nextTimeOfDayWeekday()', function () {
            it("should return a date with the same day as now given a date with a time of day after now's", function () {
                var now = new Date(2001, 1, 15);
                now.setHours(12 + 7);
                now.setMinutes(27);
                now.setSeconds(11, 0);
                now = now.next().friday();

                var timeOfDay = new Date(1981, 4, 2);
                timeOfDay.setHours(now.getHours());
                timeOfDay.setMinutes(28);
                timeOfDay.setSeconds(0, 0);

                var afterNow = dateUtil.nextTimeOfDayWeekday(timeOfDay, now);
                expect(afterNow.getHours()).toBe(timeOfDay.getHours());
                expect(afterNow.getMinutes()).toBe(timeOfDay.getMinutes());
                expect(afterNow.getSeconds()).toBe(timeOfDay.getSeconds());
                expect(afterNow.getDate()).toBe(now.getDate());
                expect(afterNow.getDay()).toBe(5);
            })

            it("should return the next Monday given now is a Friday and given a time of day before now's", function () {
                var now = new Date(2011, 6, 10);
                now.setHours(5);
                now.setMinutes(14);
                now.setSeconds(40, 0);
                now = now.next().friday();

                var timeOfDay = new Date(1970, 4, 20);
                timeOfDay.setHours(now.getHours());
                timeOfDay.setMinutes(13);
                timeOfDay.setSeconds(0, 0);

                var afterNow = dateUtil.nextTimeOfDayWeekday(timeOfDay, now);
                expect(afterNow.getHours()).toBe(timeOfDay.getHours());
                expect(afterNow.getMinutes()).toBe(timeOfDay.getMinutes());
                expect(afterNow.getSeconds()).toBe(timeOfDay.getSeconds());
                expect(afterNow.getDay()).toBe(1);
            })

            it("should return the next Monday given now is on a Saturday", function () {
                var now = new Date(2011, 6, 10);
                now.setHours(5);
                now.setMinutes(14);
                now.setSeconds(40, 0);
                now = now.next().saturday();

                var timeOfDay = new Date(1970, 4, 20);
                timeOfDay.setHours(now.getHours());
                timeOfDay.setMinutes(13);
                timeOfDay.setSeconds(0, 0);

                var afterNow = dateUtil.nextTimeOfDayWeekday(timeOfDay, now);
                expect(afterNow.getHours()).toBe(timeOfDay.getHours());
                expect(afterNow.getMinutes()).toBe(timeOfDay.getMinutes());
                expect(afterNow.getSeconds()).toBe(timeOfDay.getSeconds());
                expect(afterNow.getDay()).toBe(1);

                timeOfDay = new Date(1970, 4, 20);
                timeOfDay.at({ hour: now.getHours(), minute: 15, second: 0 });

                afterNow = dateUtil.nextTimeOfDayWeekday(timeOfDay, now);
                expect(afterNow.getHours()).toBe(timeOfDay.getHours());
                expect(afterNow.getMinutes()).toBe(timeOfDay.getMinutes());
                expect(afterNow.getSeconds()).toBe(timeOfDay.getSeconds());
                expect(afterNow.getDay()).toBe(1);
            })

            it("should behave like nextTimeOfDay on days other than Friday", function () {
                var now = new Date(2013, 4, 4, 9, 30, 0, 0).next().thursday();

                var timeOfDay = new Date(2015, 3, 3, 10, 30, 0, 0);

                var dayCall = dateUtil.nextTimeOfDay(timeOfDay, now);
                var weekdayCall = dateUtil.nextTimeOfDayWeekday(timeOfDay, now);

                expect(dayCall).toEqual(weekdayCall);
                expect(dayCall.getDate()).toEqual(now.getDate());
                expect(dayCall.getYear()).toEqual(now.getYear());
                expect(dayCall.is().thursday()).toBe(true);

                timeOfDay = new Date(2014, 7, 7, 8, 49, 0, 0);

                dayCall = dateUtil.nextTimeOfDay(timeOfDay, now);
                weekdayCall = dateUtil.nextTimeOfDayWeekday(timeOfDay, now);
                expect(dayCall).toEqual(weekdayCall);
                expect(dayCall.getDate()).toEqual(now.getDate() + 1);
                expect(dayCall.is().friday()).toBe(true);
            })
        })
    })
})
