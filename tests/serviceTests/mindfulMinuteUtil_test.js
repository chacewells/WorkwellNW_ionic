describe('mindfulMinuteUtil', function () {
    var mindfulMinuteUtil,
        array,
        element1,
        element2,
        element3,
        original,
        replacement;

    beforeEach(function () {
        module('workwellnw.services');
        inject(function (_mindfulMinuteUtil_) {
            mindfulMinuteUtil = _mindfulMinuteUtil_;
            element1 = {
                timeOfDay: new Date(1970, 0, 0)
            };
            element2 = {
                timeOfDay: new Date(1980, 0, 0)
            };
            element3 = {
                timeOfDay: new Date(1990, 0, 0)
            };
            original = {
                timeOfDay: new Date(2000, 0, 0)
            };
            replacement = {
                timeOfDay: new Date(2010, 0, 0)
            };
            array = [element1, original, element2, element3];
        });
    });

    it('should replace original with replacement at position 1', function () {
        expect(array.indexOf).toBeDefined();
        expect(array.indexOf(original)).toBe(1);
        var value = mindfulMinuteUtil.replace(array, original, replacement);
        expect(array.indexOf(replacement)).toBe(1);
        expect(value).toBe(original);
    });

    it('should merge a mindfulMinute and a template into a $cordovaLocalNotification-friendly instance', function () {
        var date = new Date(2015, 1, 15, 9, 30, 0, 0);
        var mindfulMinute = {
            timeOfDay: date
        };
        var template = {
            title: 'title man',
            message: 'text man',
            sound: 'sound.wav'
        };

        var result = mindfulMinuteUtil.toInstance(mindfulMinute, template);
        expect(result.at).toEqual(date);
        expect(result.title).toEqual('title man');
        expect(result.text).toEqual('text man');
        expect(result.sound).toEqual('sound.wav');
    })
})