describe('practiceStatsPersistence', function () {
    var practiceStatsPersistence,
        MEDITATION_PRACTICES_KEY,
        persistenceManager,
        persistedPracticeStats;

    beforeEach(function () {
        module('workwellnw.services', function ($provide) {
            $provide.factory('persistenceManager', function () {
                var service = {};

                service.persist = jasmine.createSpy('persist').and.callFake(function (key, obj) {});
                service.retrieve = jasmine.createSpy('retrieve').and.callFake(function (key) {
                    return persistedPracticeStats;
                });
                service.clear = jasmine.createSpy('clear').and.callFake(function () {});

                return service;
            });
        });

        inject(function (_practiceStatsPersistence_, _persistenceManager_, _MEDITATION_PRACTICES_KEY_) {
            MEDITATION_PRACTICES_KEY = _MEDITATION_PRACTICES_KEY_;
            practiceStatsPersistence = _practiceStatsPersistence_;
            persistenceManager = _persistenceManager_;
        });
    });

    it('should have services available', function () {
        expect(practiceStatsPersistence).toBeDefined();
        expect(persistenceManager).toBeDefined();
    });

    it('should return an empty array if persistedPracticeStats is falsey', function () {
        persistedPracticeStats = null;
        var livePracticeStats = practiceStatsPersistence.retrieve();
        expect(persistenceManager.retrieve).toHaveBeenCalled();
        expect(livePracticeStats).toEqual([]);
    });

    it('should convert weekOf properties into dates on retrieval', function () {
        var practice1 = {
            weekOf: new Date(1970, 1, 1),
            numPracticeSessions: 5,
            timePracticed: 5 * 60 * 1000,
            numGuided: 2
        };
        var practice2 = {
            weekOf: new Date(1980, 1, 1),
            numPracticeSessions: 5,
            timePracticed: 5 * 60 * 1000,
            numGuided: 2
        };
        var practice1converted = {
            weekOf: (new Date(1970, 1, 1)).toISOString(),
            numPracticeSessions: 5,
            timePracticed: 5 * 60 * 1000,
            numGuided: 2
        };
        var practice2converted = {
            weekOf: (new Date(1980, 1, 1)).toISOString(),
            numPracticeSessions: 5,
            timePracticed: 5 * 60 * 1000,
            numGuided: 2
        };
        persistedPracticeStats = [practice1converted, practice2converted];
        var expected = [practice1, practice2];
        var retrieved = practiceStatsPersistence.retrieve();
        expect(retrieved[0]).toEqual(practice1);
        expect(retrieved[0]).not.toEqual(practice1converted);
        expect(retrieved[0].weekOf.toISOString()).toEqual(practice1converted.weekOf);

        expect(retrieved[1]).toEqual(practice2);
        expect(retrieved[1]).not.toEqual(practice2converted);
    });

    it('should persist the practices to json using persistenceManager.persist', function () {
        var practice1 = {
            weekOf: new Date(1970, 1, 1),
            numPracticeSessions: 5,
            timePracticed: 5 * 60 * 1000,
            numGuided: 2
        };
        var practice2 = {
            weekOf: new Date(1980, 1, 1),
            numPracticeSessions: 5,
            timePracticed: 5 * 60 * 1000,
            numGuided: 2
        };
        var toPersist = [practice1, practice2];
        practiceStatsPersistence.save(toPersist);
        expect(persistenceManager.persist).toHaveBeenCalledWith(MEDITATION_PRACTICES_KEY, toPersist);
    });

    it('should persist an empty array when clearing the list', function () {
        practiceStatsPersistence.clear();
        expect(persistenceManager.persist).toHaveBeenCalledWith(MEDITATION_PRACTICES_KEY, []);
        expect(persistenceManager.clear).not.toHaveBeenCalled();
    });
});
