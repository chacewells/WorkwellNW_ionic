var cordova = {
    file: {
        applicationDirectory: 'applicationDirectory'
    }
};

describe('jsonResourceManager', function () {
    var $cordovaFile,
        jsonResourceManager,
        $rootScope,
        $httpBackend;

    beforeEach(function () {
        module('workwellnw.services');
        inject(function (_jsonResourceManager_, _$cordovaFile_, _$rootScope_, _$httpBackend_) {
            jsonResourceManager = _jsonResourceManager_;
            $cordovaFile = _$cordovaFile_;
            $rootScope = _$rootScope_;
            $httpBackend = _$httpBackend_;
        })
    })

    it("should call the success callback when the file's there", function () {
        var expectedData = {
            hello: "world",
            num: 42
        };
        var obj = null;
        var success = jasmine.createSpy('success').and.callFake(function (_obj) {
            obj = _obj;
        });
        $httpBackend.when('GET', 'data/somefile.json').respond(200, expectedData);
        jsonResourceManager.readResourceData('somefile').then(success, function (err) {
            console.error(err);
        });
        $httpBackend.flush();
        expect(obj).toEqual(expectedData);
    })

    it("should get the status text when the crap hits the fan", function () {
        var expectedStatusText = "Not Found",
            actualStatusText = null;
        $httpBackend.when('GET', 'data/somefile.json').respond(404, "", null, "Not Found");
        jsonResourceManager.readResourceData('somefile').then(function (data) {
            actualStatusText = data;
        }, function (err) {
            actualStatusText = err;
        });
        $httpBackend.flush();
        expect(actualStatusText).toBe(expectedStatusText);
    })
})