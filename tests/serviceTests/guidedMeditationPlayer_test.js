function AudioSpies() {
  var _AudioSpies_  = {};
  _AudioSpies_.audioPlaySpy = jasmine.createSpy('Media.play').and.callFake(function () {
    if (this.paused) {
      this.paused = false;
    }
  });
  _AudioSpies_.audioPauseSpy = jasmine.createSpy('Media.pause').and.callFake(function () {
    if (!this.paused) {
      this.paused = true;
    }
  });
  var _currentTime = 0;
  _AudioSpies_.audioGetCurrentTimeSpy = jasmine.createSpy('Media.getCurrentTime').and.callFake(function () {
    return _currentTime;
  });
  _AudioSpies_.audioStopSpy = jasmine.createSpy('Media.stop').and.callFake(function () {
    if (!this.paused) {
      this.paused = true;
      _currentTime = 0;
    }
  });
  _AudioSpies_.audioSetCurrentTimeSpy = jasmine.createSpy('Media.setCurrentTime').and.callFake(function (currentTime) {
    _currentTime = currentTime;
  });
  _AudioSpies_.audioDurationSpy = jasmine.createSpy('Media.duration').and.callFake(function () {
    return 60;
  });
  var _src = '';
  _AudioSpies_.audioGetSrcSpy = jasmine.createSpy('Media.getSrc').and.callFake(function () {
    return _src;
  });
  _AudioSpies_.audioSetSrcSpy = jasmine.createSpy('Media.setSrc').and.callFake(function (src) {
    _src = src;
  });
  var _duration = 60;
  _AudioSpies_.audioGetDurationSpy = jasmine.createSpy('Media.getDuration').and.callFake(function () {
    return _duration;
  });
  _AudioSpies_.audioSetDurationSpy = jasmine.createSpy('Media.setDuration').and.callFake(function (duration) {
    _duration = duration;
  });
  var _paused = true;
  _AudioSpies_.audioGetPausedSpy = jasmine.createSpy('Media.getPaused').and.callFake(function () {
    return _paused;
  });
  _AudioSpies_.audioSetPausedSpy = jasmine.createSpy('Media.setPaused').and.callFake(function (paused) {
    _paused = paused;
  });
  _AudioSpies_.audioReleaseSpy = jasmine.createSpy('Media.release').and.callFake(Function());
  return _AudioSpies_;
};

var Media;

describe('guidedMeditationPlayer', function () {
  var guidedMeditationPlayer,
  audioSpies;

  beforeEach(function () {
    audioSpies = AudioSpies();
    Media = jasmine.createSpy('Media').and.callFake(function (src) {
      this.play = audioSpies.audioPlaySpy;
      this.pause = audioSpies.audioPauseSpy;
      this.stop = audioSpies.audioStopSpy;
      this.release = audioSpies.audioReleaseSpy;
      Object.defineProperties(this, {
        "paused": {
          "get": audioSpies.audioGetPausedSpy,
          "set": audioSpies.audioSetPausedSpy
        },
        "duration": {
          "get": audioSpies.audioGetDurationSpy,
          "set": audioSpies.audioSetDurationSpy
        },
        "currentTime": {
          "get": audioSpies.audioGetCurrentTimeSpy,
          "set": audioSpies.audioSetCurrentTimeSpy
        },
        "src": {
          "get": audioSpies.audioGetSrcSpy,
          "set": audioSpies.audioSetSrcSpy
        }
      });
    });
    module('workwellnw.services');
    inject(function (_guidedMeditationPlayer_) {
      guidedMeditationPlayer = _guidedMeditationPlayer_;
    });
  })

  it('should make use of the Media constructor', function () {
    expect(guidedMeditationPlayer).toBeDefined();
    var player = guidedMeditationPlayer.newPlayer('some_id_meditation.mp3');
    expect(Media).toBeDefined();
    expect(Media).toHaveBeenCalledWith(
      'documents://some_id_meditation.mp3',
      jasmine.any(Function),
      jasmine.any(Function),
      jasmine.any(Function)
    );
  })

  it('should play and pause', function () {
    var player = guidedMeditationPlayer.newPlayer('some_id');
    player.play();
    expect(audioSpies.audioPlaySpy).toHaveBeenCalled();
    expect(audioSpies.audioSetPausedSpy).toHaveBeenCalledWith(false);
    expect(audioSpies.audioSetPausedSpy.calls.count()).toBe(1);

    player.pause();
    expect(audioSpies.audioPauseSpy).toHaveBeenCalled();
    expect(audioSpies.audioSetPausedSpy).toHaveBeenCalledWith(true);
    expect(audioSpies.audioSetPausedSpy.calls.count()).toBe(2);
  })

  it('should also stop', function () {
    var player = guidedMeditationPlayer.newPlayer('some_id');
    player.play();
    expect(audioSpies.audioPlaySpy).toHaveBeenCalled();
    expect(audioSpies.audioSetPausedSpy).toHaveBeenCalledWith(false);
    expect(audioSpies.audioSetPausedSpy.calls.count()).toBe(1);

    player.stop();
    expect(audioSpies.audioStopSpy).toHaveBeenCalled();
  })

  it('should also clean up', function () {
    var player = guidedMeditationPlayer.newPlayer('some_id');
    player.cleanup();
    expect(audioSpies.audioSetSrcSpy).not.toHaveBeenCalled();
  })
})
