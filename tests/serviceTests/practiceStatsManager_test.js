describe('practiceStatsManager', function () {
  var practiceStatsPersistence,
      practiceStatsManager,
      weekOne,
      persistedStats,
      dateUtil,
      MEDITATION_PRACTICES_KEY;

  beforeEach(function () {
    module('workwellnw.services', function ($provide) {
      $provide.factory('practiceStatsPersistence', function () {
        var service = {};
        service.retrieve = jasmine.createSpy('practiceStatsPersistence.retrieve').and.callFake(function () {
          return persistedStats;
        });
        service.save = jasmine.createSpy('practiceStatsPersistence.save');
        service.clear = jasmine.createSpy('practiceStatsPersistence.clear');
        return service;
      })
    });

    weekOne = {
      weekOf: (new Date()).set({month:2, day: 5, year: 2015, hour: 0, minute: 0, second: 0}),
      numPracticeSessions: 5,
      numGuided: 3,
      timePracticed: (55 * 60 * 1000)
    };
    persistedStats = [weekOne];

    inject(function (_practiceStatsPersistence_, _practiceStatsManager_, _MEDITATION_PRACTICES_KEY_, _dateUtil_) {
      practiceStatsPersistence = _practiceStatsPersistence_;
      practiceStatsManager = _practiceStatsManager_;
      MEDITATION_PRACTICES_KEY = _MEDITATION_PRACTICES_KEY_;
      dateUtil = _dateUtil_;
    })
  })

  it('should retrieve a list of persisted stats when it initializes', function () {
    expect(practiceStatsPersistence.retrieve).toHaveBeenCalled();
  })

  it('should add some milliseconds to the current week', function () {
    spyOn(dateUtil, 'currentWeekOf').and.returnValue(weekOne.weekOf);
    spyOn(dateUtil, 'nextWeekOf').and.returnValue(weekOne.weekOf.next().sunday());
    var timeSpent = (4*60 + 25) * 1000; // 4:25
    var originalTimePracticed = weekOne.timePracticed;
    practiceStatsManager.addOne(timeSpent, false);
    expect(weekOne.timePracticed).toBe(originalTimePracticed + timeSpent);
    expect(weekOne.numPracticeSessions).toBe(6);
    expect(weekOne.numGuided).toBe(3);
    expect(dateUtil.currentWeekOf).toHaveBeenCalled();
    expect(dateUtil.nextWeekOf).toHaveBeenCalled();
  })

  it('should add a stat to the list', function () {
    expect(persistedStats.length).toBe(1);
    practiceStatsManager.addOne(50, false);
    expect(persistedStats.length).toBe(2);
    expect(persistedStats[1].numPracticeSessions).toBe(1);
    expect(persistedStats[1].numGuided).toBe(0);
    expect(persistedStats[1].timePracticed).toBe(50);
  })

  it("should save its stats when it's flushed", function () {
    expect(practiceStatsPersistence.save).not.toHaveBeenCalled();
    practiceStatsManager.addOne(60, true);
    practiceStatsManager.flush();
    expect(practiceStatsPersistence.save).toHaveBeenCalledWith(persistedStats);
  })
})
