describe('mindfulMinutePersistence', function () {
    var mindfulMinutePersistence,
        MINDFUL_MINUTES_KEY,
        persistenceManager,
        persistedMindfulMinutes;

    beforeEach(function () {
        module('workwellnw.services', function ($provide) {
            $provide.factory('persistenceManager', function () {
                var service = {};

                service.persist = jasmine.createSpy('persist').and.callFake(function (key, obj) {});
                service.retrieve = jasmine.createSpy('retrieve').and.callFake(function (key) {
                    return persistedMindfulMinutes;
                });
                service.clear = jasmine.createSpy('clear').and.callFake(function () {});

                return service;
            });
        });

        inject(function (_mindfulMinutePersistence_, _persistenceManager_, _MINDFUL_MINUTES_KEY_) {
            MINDFUL_MINUTES_KEY = _MINDFUL_MINUTES_KEY_;
            mindfulMinutePersistence = _mindfulMinutePersistence_;
            persistenceManager = _persistenceManager_;
        });
    });

    it('should have services available', function () {
        expect(mindfulMinutePersistence).toBeDefined();
        expect(persistenceManager).toBeDefined();
    });

    it('should return an empty array if mindfulMinutes is falsey', function () {
        persistedMindfulMinutes = null;
        var liveMindfulMinutes = mindfulMinutePersistence.retrieve();
        expect(persistenceManager.retrieve).toHaveBeenCalled();
        expect(liveMindfulMinutes).toEqual([]);
    });

    it('should convert timeOfDay properties into dates on retrieval', function () {
        var mm1 = {
            timeOfDay: new Date(1970, 1, 1)
        };
        var mm2 = {
            timeOfDay: new Date(1980, 1, 1)
        };
        var mm1converted = {
            timeOfDay: (new Date(1970, 1, 1)).toISOString()
        };
        var mm2converted = {
            timeOfDay: (new Date(1980, 1, 1)).toISOString()
        };
        persistedMindfulMinutes = [mm1converted, mm2converted];
        var expected = [mm1, mm2];
        var retrieved = mindfulMinutePersistence.retrieve();
        expect(retrieved[0]).toEqual(mm1);
        expect(retrieved[0]).not.toEqual(mm1converted);
        expect(retrieved[0].timeOfDay.toISOString()).toEqual(mm1converted.timeOfDay);

        expect(retrieved[1]).toEqual(mm2);
        expect(retrieved[1]).not.toEqual(mm2converted);
    });

    it('should persist the mindfulminutes to json using persistenceManager.persist', function () {
        var mm1 = {
            timeOfDay: new Date(1970, 1, 1)
        };
        var mm2 = {
            timeOfDay: new Date(1980, 1, 1)
        };
        var toPersist = [mm1, mm2];
        mindfulMinutePersistence.save(toPersist);
        expect(persistenceManager.persist).toHaveBeenCalledWith(MINDFUL_MINUTES_KEY, toPersist);
    });

    it('should persist an empty array when clearing the list', function () {
        mindfulMinutePersistence.clear();
        expect(persistenceManager.persist).toHaveBeenCalledWith(MINDFUL_MINUTES_KEY, []);
        expect(persistenceManager.clear).not.toHaveBeenCalled();
    });
});