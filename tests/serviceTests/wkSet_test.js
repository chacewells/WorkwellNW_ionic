describe('wkSet', function () {
  var wkSet,
  set;

  beforeEach(function () {
    module('workwellnw.services');
    inject(function (_wkSet_) {
      wkSet = _wkSet_;
    })
    set = 0x0;
  })

  it('should set sunday to the first bit', function () {
    var sunday = 0;
    set = wkSet.putAt(set, sunday);
    expect(set).toBe(0x1);
  })
  it('should set monday to the second bit', function () {
    var monday = 1;
    set = wkSet.putAt(set, monday);
    expect(set).toBe(0x2);
  })
  it('should set tuesday to the third bit', function () {
    var tuesday = 2;
    set = wkSet.putAt(set, tuesday);
    expect(set).toBe(0x4);
  })
  it('should set wednesday to the fourth bit', function () {
    var wednesday = 3;
    set = wkSet.putAt(set, wednesday);
    expect(set).toBe(0x8);
  })
  it('should set thursday to the fifth bit', function () {
    var thursday = 4;
    set = wkSet.putAt(set, thursday);
    expect(set).toBe(0x10);
  })
  it('should set friday to the sixth bit', function () {
    var friday = 5;
    set = wkSet.putAt(set, friday);
    expect(set).toBe(0x20);
  })
  it('should set saturday to the seventh bit', function () {
    var saturday = 6;
    set = wkSet.putAt(set, saturday);
    expect(set).toBe(0x40);
  })
  it('should set sunday to the first bit', function () {
    set = wkSet.put(set, wkSet.SUNDAY);
    expect(set).toBe(0x1);
  })
  it("should count three days for sun-tues-sat", function () {
    set = wkSet.put(set, wkSet.SUNDAY);
    set = wkSet.put(set, wkSet.TUESDAY);
    set = wkSet.put(set, wkSet.SATURDAY);
    var count = wkSet.count(set);
    expect(count).toBe(3);
  })
  it("should count seven days for all days set", function () {
    set = wkSet.ALL;
    var count = wkSet.count(set);
    expect(count).toBe(7);
  })
  it("should drop the third bit when dropping tuesday", function () {
    set = wkSet.ALL;
    set = wkSet.drop(set, wkSet.TUESDAY);
    expect(wkSet.count(set)).toBe(6);
  })
})
