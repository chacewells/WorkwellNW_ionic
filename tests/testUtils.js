function ionicPlatformProvider ($provide) {
  $provide.factory('$ionicPlatform', function ($q) {
    var service = {};
    service.ready = jasmine.createSpy('ready').and.callFake(function (cb) {
      var deferred = $q.defer();
      cb && cb();
      return deferred.promise;
    });
    return service;
  });
}
