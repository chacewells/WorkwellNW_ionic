describe('MindfulMinuteListController', function () {
    var $controller,
        MindfulMinuteListController,
        $scope,
        $cordovaDatePicker,
        mindfulMinuteManager,
        mindfulMinuteUtil,
        newMindfulMinute,
        findMindfulMinute,
        removeMindfulMinute,
        updatedMindfulMinute,
        editingMindfulMinute,
        cancelledMindfulMinute,
        allMindfulMinutes;

    beforeEach(function () {
        newMindfulMinute = {
            timeOfDay: new Date(1970, 0, 0)
        };
        findMindfulMinute = {
            timeOfDay: new Date(1990, 0, 0)
        };
        removeMindfulMinute = {
            timeOfDay: new Date(1980, 0, 0)
        };
        updatedMindfulMinute = {
            timeOfDay: new Date(2000, 0, 0)
        };
        editingMindfulMinute = {
            timeOfDay: new Date(2010, 0, 0)
        };
        allMindfulMinutes = [newMindfulMinute, findMindfulMinute, removeMindfulMinute, updatedMindfulMinute];
    });

    beforeEach(module('workwellnw', function ($provide) {
        $provide.factory('mindfulMinuteManager', function () {
            var service = {};
            service.create = jasmine.createSpy('create').and.returnValue(newMindfulMinute);
            service.all = jasmine.createSpy('all').and.returnValue(allMindfulMinutes);
            service.update = jasmine.createSpy('update').and.callFake(function (updated) {});
            return service;
        });
        $provide.factory('mindfulMinuteUtil', function () {
            var service = {};
            service.replace = jasmine.createSpy('replace').and.callFake(function () {});
            return service;
        });
    }));

    beforeEach(inject(function (_$controller_, $rootScope, $httpBackend, _mindfulMinuteManager_, _mindfulMinuteUtil_, _$cordovaDatePicker_, $q) {
        $controller = _$controller_;
        $scope = $rootScope.$new();
        mindfulMinuteManager = _mindfulMinuteManager_;
        mindfulMinuteUtil = _mindfulMinuteUtil_;
        $httpBackend.whenGET(/templates\/.*/).respond(200, '');
        $httpBackend.whenGET('data/templates.json').respond(200);
        var deferred = $q.defer();

        var promise = deferred.promise;
        $cordovaDatePicker = _$cordovaDatePicker_;
        spyOn($cordovaDatePicker, 'show').and.returnValue(promise);
        MindfulMinuteListController = $controller('MindfulMinuteListController', {
            $scope: $scope
        });
    }));

    describe('mindfulMinuteManager', function () {
        it('should update when i say "save"', function () {
            spyOn(allMindfulMinutes, 'push').and.callThrough();
            $scope.$apply();
            $scope.$emit('mmsave', newMindfulMinute);
            expect(mindfulMinuteManager.save).toBeUndefined();
            expect(mindfulMinuteManager.update).toHaveBeenCalledWith(allMindfulMinutes);
            expect(allMindfulMinutes.push).toHaveBeenCalledWith(newMindfulMinute);
        });

        it('should update when I remove something', function () {
            $scope.$apply();
            expect(mindfulMinuteManager.update).not.toHaveBeenCalled();
            $scope.removeMindfulMinute(removeMindfulMinute);
            expect(mindfulMinuteManager.update).toHaveBeenCalled();
        });

        it('should update when I update a single mindful minute', function () {
            $scope.$apply();
            expect(mindfulMinuteManager.update).not.toHaveBeenCalled();
            var index = allMindfulMinutes.indexOf(updatedMindfulMinute);
            var newDate = new Date(2001, 0, 0);
            updatedMindfulMinute.timeOfDay = newDate;
            $scope.$emit('mmupdate', updatedMindfulMinute);

            expect(mindfulMinuteManager.update).toHaveBeenCalled();
            expect(allMindfulMinutes[index].timeOfDay).toBe(newDate);
        });
    });

    describe('mindfulMinuteUtil', function () {
        it('should call replace when cancelled', function () {
            $scope.$apply();
            expect(mindfulMinuteUtil.replace).not.toHaveBeenCalled();
            $scope.edit(editingMindfulMinute);
            expect($scope.editing).toBe(editingMindfulMinute);
            expect($scope.original.timeOfDay).toEqual(editingMindfulMinute.timeOfDay);
            expect($scope.original).toEqual(editingMindfulMinute);
            expect($scope.original).not.toBe($scope.editing);
            var options = {
                edited: editingMindfulMinute,
                original: $scope.original
            };
            $scope.mmcancel(options);
            expect(mindfulMinuteUtil.replace).toHaveBeenCalledWith(allMindfulMinutes, options.edited, options.original);
        });
    });

});
