describe('GuidedMeditationPlayerController', function () {
  var controller,
      $rootScope,
      $scope,
      $log,
      $timeout,
      $ionicPopup,
      $cordovaFile,
      $q,
      GuidedMeditationPlayerController,
      guidedMeditationPlayer,
      player,
      gm = { title: 'title', id: 'id' },
      meditationStart,
      meditationBreak,
      meditationDone,
      meditationTracker;

  beforeEach(function () {
    meditationStart = jasmine.createSpy('meditationStart')
    meditationBreak = jasmine.createSpy('meditationBreak'),
    meditationDone = jasmine.createSpy('meditationDone');
    meditationTracker = function () {
      $rootScope.$on('meditation:start', meditationStart);
      $rootScope.$on('meditation:break', function (event, guided) {
        meditationBreak(guided);
      });
      $rootScope.$on('meditation:done', function (event, guided) {
        meditationDone(guided);
      });
    };

    module('ionic', 'workwellnw.services', 'workwellnw.controllers');
    inject(function (
        _$controller_,
        _$rootScope_,
        _guidedMeditationPlayer_,
        _$log_,
        _$timeout_,
        _$cordovaFile_,
        _$q_) {
      controller = _$controller_;
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      guidedMeditationPlayer = _guidedMeditationPlayer_;
      $log = _$log_;
      $timeout = _$timeout_;
      $cordovaFile = _$cordovaFile_;
      $q = _$q_;
    })
    spyOn($scope, '_').and.returnValue({
      filter: jasmine.createSpy('_.filter').and.returnValue([gm])
    })
    spyOn(guidedMeditationPlayer, 'newPlayer').and.callFake(function () {
      player = {
        play: jasmine.createSpy('player.play').and.callFake(function () {this.paused = false}),
        pause: jasmine.createSpy('player.pause').and.callFake(function () {this.paused = true}),
        stop: jasmine.createSpy('player.stop').and.callFake(function () {this.pause();}),
        cleanup: jasmine.createSpy('player.cleanup').and.callFake(function () {this.pause();}),
        currentTime: 0,
        duration: 60,
        paused: true
      };
      return player;
    });
  })

  it('should start the player when I call $scope.play', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeSuccess', {name: 'tabs.guidedmeditationPlayer'}, gm, {name:''}, {});
    expect(player).toBeDefined();
    expect(player.play).toHaveBeenCalled();
    expect(player.paused).toBe(false);
  })

  it('should also pause', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeSuccess', {name: 'tabs.guidedmeditationPlayer'}, gm, {name:''}, {});
    $scope.play();
    expect(player.play).toHaveBeenCalled();
    expect(player.paused).toBe(true);
  })

  it('should also stop', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeSuccess', {name: 'tabs.guidedmeditationPlayer'}, gm, {name:''}, {});
    $scope.stop();
    expect(player.pause).toHaveBeenCalled();
    expect(player.paused).toBe(true);
  })

  it('should also clean up', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeSuccess', {name: 'tabs.guidedmeditationPlayer'}, gm, {name:''}, {});
    expect(player.play).toHaveBeenCalled();
    expect(player.paused).toBe(false);
    $rootScope.$broadcast('$stateChangeStart', {name:''}, gm, {name: 'tabs.guidedmeditationPlayer'}, {});
    expect(player.cleanup).toHaveBeenCalled();
    expect(player.cleanup.calls.count()).toBe(1);

    $rootScope.$broadcast('$stateChangeStart', {name:''}, gm, {name: 'tabs.guidedmeditationPlayer'}, {});
    expect(player.cleanup.calls.count()).toBe(1); // this means 'player' in $scope's function scope is null
  })

  it('should trigger meditation:start in $scope.play()', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    meditationTracker();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeSuccess', {name: 'tabs.guidedmeditationPlayer'}, gm, {name:''}, {});
    expect(meditationStart).toHaveBeenCalled();
  })

  it('should trigger meditation:break in $scope.pause()', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    meditationTracker();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeSuccess', {name: 'tabs.guidedmeditationPlayer'}, gm, {name:''}, {});
    expect(meditationBreak).not.toHaveBeenCalled();
    $scope.play();
    expect(meditationBreak).toHaveBeenCalledWith(true);
  })

  it('should trigger meditation:break in $scope.stop()', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    meditationTracker();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeSuccess', {name: 'tabs.guidedmeditationPlayer'}, gm, {name:''}, {});
    expect(meditationBreak).not.toHaveBeenCalled();
    $scope.stop();
    expect(meditationBreak).toHaveBeenCalledWith(true);
  })

  it('should trigger meditation:done when view exited', function (done) {
    spyOn($cordovaFile, 'checkFile').and.returnValue($q.defer().promise);
    done();
    meditationTracker();
    GuidedMeditationPlayerController = controller('GuidedMeditationPlayerController', {$scope: $scope});
    $rootScope.$broadcast('$stateChangeStart', {name:''}, gm, {name: 'tabs.guidedmeditationPlayer'}, {});
    expect(meditationDone).toHaveBeenCalledWith(true);
  })
})
