describe('MeditationTimerController', function () {
  var controller,
      MeditationTimerController,
      meditationTimer,
      timer,
      $scope,
      $rootScope,
      pacer,
      cb;

  beforeEach(function () {
    module('ionic', 'workwellnw.services', 'workwellnw.controllers');
    inject(function (_$controller_, _$rootScope_, _meditationTimer_) {
      controller = _$controller_;
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new(false);
      meditationTimer = _meditationTimer_;
    })

    spyOn(meditationTimer, 'withSound').and.callFake(function (sound) {
      timer = {
        start: jasmine.createSpy('timer.start').and.callFake(function (_cb, _pacer) {
          pacer = _pacer;
          cb = _cb;
          this.running = true;
          this.done = false;
        }),
        stop: jasmine.createSpy('timer.stop').and.callFake(function () {
          this.running = false;
          this.done = true;
        }),
        pause: jasmine.createSpy('timer.pause').and.callFake(function () { this.running = false; }),
        setTime: jasmine.createSpy('timer.setTime').and.callFake(Function()),
        whenDone: jasmine.createSpy('timer.whenDone').and.callFake(Function()),
        resume: jasmine.createSpy('timer.resume').and.callFake(Function()),
        running: false,
        done: true
      };
      return timer;
    })
  })

  it('should initialize the timer', function () {
    MeditationTimerController = controller('MeditationTimerController', {$scope: $scope});
    expect(meditationTimer.withSound).toHaveBeenCalledWith('ping');
    expect(timer.start).not.toHaveBeenCalled();
    expect(timer.whenDone).toHaveBeenCalledWith(jasmine.any(Function));
  })

  it('should start the timer when I call $scope.start', function () {
    MeditationTimerController = controller('MeditationTimerController', {$scope: $scope});
    expect(timer.start).not.toHaveBeenCalled();
    $scope.start();
    expect(timer.start).toHaveBeenCalled();
  })

  it('should start the timer with the right values', function () {
    MeditationTimerController = controller('MeditationTimerController', {$scope: $scope});
    $scope.config.bellInterval = 20;
    $scope.config.bellUnit = 1;
    $scope.start();
    expect(timer.start).toHaveBeenCalledWith(jasmine.any(Function), 20);
    $scope.stop(); // should flush the timer

    $scope.config.bellInterval = 2;
    $scope.config.bellUnit = 60;
    $scope.start();
    expect(timer.start).toHaveBeenCalledWith(jasmine.any(Function), 120);
  })

  it('should resume the timer when the timer is running and I call $scope.start', function () {
    MeditationTimerController = controller('MeditationTimerController', {$scope: $scope});
    timer.done = false;
    $scope.start();
    expect(timer.resume).toHaveBeenCalled;
  })

  it("should report accurate duration times", function () {
    MeditationTimerController = controller('MeditationTimerController', {$scope: $scope});
    $scope.timeRemaining.hours = 1;
    $scope.timeRemaining.minutes = 1;
    $scope.timeRemaining.seconds = 1;
    var expectedDuration = 3661;
    expect($scope.getDuration()).toEqual(expectedDuration);
  })

})
