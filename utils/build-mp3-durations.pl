#!/usr/bin/perl
use v5.18;
use Getopt::Long;
use File::Spec::Functions;
use MP3::Info;
use POSIX;
use File::Slurper qw(write_text);
use File::Basename;
use JSON;

GetOptions(
    'sourcedir:s' => \my $sourcedir,
    help          => \my $help,
);

if ($help) { print usage(); exit }

# directory paths and work file names
my $PROJECT = eval { shift || $ENV{WORKWELLNW_HOME} }
    or die "This script really doesn't make sense without the project's home directory\n" . usage();
-d (my $MEDIA = catdir $PROJECT, 'www', 'media')
    or die "Media directory not found\n".usage();
-d ($sourcedir //= $MEDIA)
    or die "Source directory not found\n".usage();
my $DURATIONS_FN = catfile $MEDIA, 'durations.json';

# the media files
my @media_files = glob catfile($sourcedir, '*.mp3')
    or die "No files to check duration";

my %durations =
map {
    my $basename = basename $_;
    my $duration = floor get_mp3info($_)->{SECS};
    ($basename => $duration)
} @media_files;

write_text( $DURATIONS_FN, JSON->new->pretty->encode(\%durations) );

sub usage {
    my $usage_str = <<"    USAGE";
    usage: $0 [--sourcedir SOURCEDIR] PROJECT

    determines the total playing time of each .mp3 file in SOURCEDIR and stores them in PROJECT/www/media/checksums.json
    looks in ./www/media if SOURCEDIR is not supplied
    USAGE
    $usage_str =~ s/    //g;
    $usage_str
}
