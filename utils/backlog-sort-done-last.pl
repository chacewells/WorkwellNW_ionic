#!/usr/bin/perl
use v5.18;
use JSON;
use File::Copy;

my $backlog_fn = "backlog.json";

# backup the backlog
{
my $backlog_bak = "$backlog_fn.bak";
copy $backlog_fn, $backlog_bak or die "Couldn't backup '$backlog_fn': $!";
}

# sort the data
my $raw = do { local $/ = undef; open my $backlog, "<", "backlog.json" or die $!; <$backlog> };
my $json = JSON->new;
$json->canonical(1);
my $data = $json->decode($raw);
my @sorted = map { $data->[$_] } sort { $data->[$a]{done} <=> $data->[$b]{done} } 0 .. $#{$data};

# write to the original file
open my $backlog, ">", $backlog_fn or die "Couldn't open '$backlog_fn' for writing: $!";
say {$backlog} $json->pretty->encode(\@sorted);
close $backlog;
