#!/usr/bin/perl
use v5.18;
use JSON qw/from_json/;
use File::Slurper qw/read_text/;
use Getopt::Long;
use constant {
  BACKLOG_FILE => './backlog.json',
};

our @SORTS = qw(area feature done);

our $backlog;

GetOptions(
  'area:s'    => \our $area,
  'feature:s' => \our $feature,
  'done:i'    => \our $done,
  'sortby:s'  => \our $sortby,
  'desc'      => \our $desc,
);

$sortby //= 'feature';

&fetch_backlog;

my @filters = (
  sub {
    return @_ unless $area;
    grep { $_->{area} =~ /$area/ } @_
  },
  sub {
    return @_ unless $feature;
    grep { $_->{feature} =~ /$feature/ } @_
  },
  sub {
    return @_ unless defined $done;
    grep { $_->{done} == $done } @_
  },
  sub {
    my $order = $desc ? -1 : 1; # ascending order by default
    local $" = ', ';
    die "can only sort by one of @SORTS" unless grep { $_ eq $sortby } @SORTS;
    sort { $desc * ( $_->{$sortby}{$a} cmp $_->{$sortby}{$b} ) } @_
  }
);

&format_print for apply_filters( \@filters, @$backlog );

sub apply_filters {
  my @filters = @{ shift() };
  if (@filters) {
    my $next_filter = shift @filters;
    apply_filters(\@filters, $next_filter->(@_));
  } else { @_ }
}

sub format_print {
  my $format = '';
  for my $k (sort keys %$_) {
    $format .= sprintf('%-10s:', $k) . " %s\n"
  }
  $format .= '=' x 60 . "\n";
  $_->{done} = $_->{done} ? "YES" : "NO";
  printf $format, @$_{sort keys %$_};
}

sub fetch_backlog {
  my $raw = read_text BACKLOG_FILE or die "can't find backlog file: $!";
  $backlog = from_json $raw;
}
