#!/usr/bin/perl
use v5.18;
use Getopt::Long;
use Digest::MD5 qw(md5_hex);
use File::Spec::Functions;
use File::Slurper qw(read_binary write_text);
use File::Basename;
use JSON;

GetOptions(
    'sourcedir:s' => \my $sourcedir,
    help          => \my $help,
);

if ($help) { print usage(); exit }

# directory paths and work file names
my $PROJECT = eval { shift || $ENV{WORKWELLNW_HOME} }
    or die "This script really doesn't make sense without the project's home directory\n" . usage();
-d (my $MEDIA = catdir $PROJECT, 'www', 'media')
    or die "Media directory not found\n" . usage();
-d ($sourcedir //= $MEDIA)
    or die "Source directory not found\n" . usage();
my $CHK_FN = catfile $MEDIA, 'checksums.json';

# the media files
my @media_files = glob catfile($sourcedir, '*.mp3')
    or die "No files to checksum";

my %checksums =
map {
    my $basename = basename $_;
    my $md5sum = md5_hex read_binary $_;
    ($basename => $md5sum)
} @media_files;

write_text( $CHK_FN, JSON->new->pretty->encode(\%checksums) );

sub usage {
    <<"    USAGE"  =~ s/^\s{4}//mgr;
    usage: $0 [--sourcedir SOURCEDIR] PROJECT

    creates checksums of the .mp3 files in SOURCEDIR and stores them in PROJECT/www/media/checksums.json
    looks in ./www/media if SOURCEDIR is not supplied
    USAGE
}
