angular.module('workwellnw.directives')

.directive('playButton', [function () {
  return {
    scope: true,
    restrict: 'AE',
    replace: 'true',
    template: '<button class="button button-positive"><i class="icon icon-large" ng-class="iconStyle"></i></button>',
    link: function (scope, elem, attrs) {
      scope.iconStyle = 'ion-play';
      scope.$watch('running', function (running, before) {
        if (before === running) { // no change
          return;
        } else if (running) { // change to true
          scope.iconStyle = 'ion-pause';
        } else { // change to false
          scope.iconStyle = 'ion-play';
        }
      });
    }
  };
}])
