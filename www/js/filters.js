angular.module('workwellnw.filters', [])

.filter('mmSaveActionDisplay', function () {
    return function (action) {
        var result;
        if (typeof action === 'undefined') {
            result = '';
        } else {
            result = action
                .replace(/^mm/, '')
                .replace(/^[a-z]/, function (str) {
                    return str.toUpperCase();
                });
        }

        return result;
    }
})

.filter('hmsString', ['hmsFactory', function (hmsFactory) {
  return function (seconds) {
    var hms = hmsFactory(0,0,Number(seconds));
    var h = hms.hours, m = hms.minutes, s = Math.floor(hms.seconds);
    var result = '';
    result += h ? ((h > 9 ? h : '0' + h) + ':') : '';
    result += (m > 9 ? m : '0' + m) + ':';
    result += s > 9 ? s : '0' + s;
    return result;
  };
}])

.filter('mtos', function () {
  return function (millis) {
    return Math.round(millis / 1000);
  }
})

.filter('range', ['$filter', function ($filter) {
    return function (start, end) {
      return $filter('step')(start, 1, end);
    };
}])

.filter('step', function () {
  return function (start, step, end) {
    var result = [];
    for (var i = start; i <= end; i += step) {
      result.push(i);
    }
    return result;
  };
})
