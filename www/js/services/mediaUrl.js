angular.module('workwellnw.services')

.factory('mediaUrl', [function () {
  var service = {};
  var mediaDir = 'file://media/';

  function extension (name, ext) {
    return name + '.' + ext;
  }

  service.mp3 = function (name) {
    return extension(mediaDir + name, 'mp3');
  };

  return service;
}])
