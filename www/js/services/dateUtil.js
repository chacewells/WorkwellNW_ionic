angular.module('workwellnw.services')

.factory('dateUtil', [ 'DayConstants', 'TimeConstants',
function (DayConstants, TimeConstants) {

  var service = {};
    /**
     * Produces a date consisting of the hours, minutes and seconds of 'from' and
     * everything else from 'to'.
     * @param from The date where hours, minutes, and seconds are sourced.
     * @param to The date where everything else comes from.
     * @return The date resulting from the transfer (this function is side-effect free).
     */
    service.transferHMS = function (from, to) {
        var result = new Date(to);

        result.setHours(from.getHours());
        result.setMinutes(from.getMinutes());
        result.setSeconds(from.getSeconds(), 0);

        return result;
    };

    /**
     * Given two dates, produces the first available date after the second having
     * the same hours, minutes, and seconds as the first date.
     * @param date The date providing the hours, minutes, and seconds.
     * @param now The date after which the produced date will take place.
     */
    service.nextTimeOfDay = function (date, now) {
        var newDate = new Date(now.getTime());
        newDate = this.transferHMS(date, newDate);

        if ( newDate.getTime() <= now.getTime() ) {
            newDate.setTime(newDate.getTime() + TimeConstants.DAY);
        }

        return newDate;
    };

    /**
     * Given two dates, produces the first available weekday date after the second having
     * the same hours, minutes, and seconds as the first date.
     * @param date The date providing the hours, minutes, and seconds.
     * @param now The date after which the produced date will take place.
     */
    service.nextTimeOfDayWeekday = function (date, now) {
        var newDate = new Date(now.getTime());
        newDate = this.transferHMS(date, newDate);

        if (newDate.getDay() === DayConstants.SATURDAY ||
            newDate.getDay() === DayConstants.SUNDAY) {
          newDate = this.nextMonday(newDate);
        } else if ( newDate.getTime() <= now.getTime() ) {
            if ( newDate.getDay() === DayConstants.FRIDAY ) {
                newDate = this.nextMonday(newDate);
            } else {
              // adding 1 day to Sunday regardless of whether on weekend or not
              newDate = new Date( newDate.getTime() + TimeConstants.DAY );
            }
        }

        return newDate;
    };

    /**
     * Return the next Monday from the given date.
     * @param date The date
     * @return A date with the same time as date, but next Monday.
     */
    service.nextMonday = function (date) {
        var newDate, newTime;
        var time = date.getTime();
        var day = date.getDay();
        if (day === DayConstants.SUNDAY) {
            newTime = time + TimeConstants.DAY; // add a day
            newDate = new Date(newTime);
        } else {
            newTime = time + ( (DayConstants.MONDAY - day) + 7 ) * TimeConstants.DAY;
            newDate = new Date(newTime);
        }

        return newDate;
    }

    /**
     * Returns the Sunday beginning this week.
     * Significantly longer than the one-liner it used to be, but now it doesn't depend on the 35MB datejs library
     */
    service.currentWeekOf = function () {
        var today = new Date();
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0, 0);

        var todayTime = today.getTime();
        var nowDayOfWeek = today.getDay();
        var currentWeekTime = todayTime - (nowDayOfWeek * TimeConstants.DAY); // subtract (millis/day)*todayTime to get the beginning of the week
        return new Date(currentWeekTime);
    };
    
    /**
     * Returns the Sunday beginning next week.
     * A bit longer than the one-liner it used to be, but now it doesn't depend on the 35MB datejs library
     */
    service.nextWeekOf = function () {
        var nextWeekTime = this.currentWeekOf().getTime() + TimeConstants.WEEK; // get the current week and add a week
        return new Date(nextWeekTime);
    };

    service.is = function (date) {
      return {
        between: function (a, b) {
          return ( date.getTime() >= a.getTime() && date.getTime() <= b.getTime() ) ||
                 ( date.getTime() <= a.getTime() && date.getTime() >= b.getTime() );
        }
      };
    };

    service.now = function () {
        var date = new Date();
        date.setSeconds(date.getSeconds(), 0);
        return date;
    };

    return service;
}])
