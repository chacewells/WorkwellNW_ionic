angular.module('workwellnw.services')

.factory('mindfulMinutePersistence',
['persistenceManager', 'MINDFUL_MINUTES_KEY',
function (persistenceManager, MINDFUL_MINUTES_KEY) {

    function retrieve() {
        var mindfulMinutes = persistenceManager.retrieve(MINDFUL_MINUTES_KEY);
        if (typeof mindfulMinutes === 'undefined' || !mindfulMinutes) {
            mindfulMinutes = [];
        }

        angular.forEach(mindfulMinutes, function (mindfulMinute, idx) {
            var timeOfDay = new Date(mindfulMinute.timeOfDay);
            mindfulMinutes[idx] = {
                timeOfDay: timeOfDay
            };
        });

        return mindfulMinutes;
    }

    function save(mindfulMinutes) {
        persistenceManager.persist(MINDFUL_MINUTES_KEY, mindfulMinutes);
    }

    function clear() {
        persistenceManager.persist(MINDFUL_MINUTES_KEY, []);
    }

    return {
        retrieve: retrieve,
        save: save,
        clear: clear
    };
}])
