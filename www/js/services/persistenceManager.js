angular.module('workwellnw.services')

.factory('persistenceManager', ['$window', function ($window) {
    /* TODO suspicious b/c persistent; find out if localStorage depends on
     * platform ready (probably not)
     */
    var storage = $window.localStorage;

    function persist(key, obj) {
        var jsonObj = angular.toJson(obj);
        storage.setItem(key, jsonObj);
    }

    function retrieve(key) {
        var jsObj;

        var retrievedValue = storage.getItem(key);
        if (typeof retrievedValue !== undefined && typeof retrievedValue === 'string' && retrievedValue) {
            jsObj = (retrievedValue === 'undefined') ? null : angular.fromJson(retrievedValue);
        } else {
            jsObj = null;
        }

        return jsObj;
    }

    function clear() {
        storage.clear();
    }

    return {
        persist: persist,
        retrieve: retrieve,
        clear: clear
    };
}])
