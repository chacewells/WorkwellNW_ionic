angular.module('workwellnw.services')
.factory('guidedMeditationPlayer',
function ($log, $timeout, mp3durations) {
  var service = {};

  service.newPlayer = function (filename) {
    var player = {};
    var pausedStatus = true;
    var currentTime = 0;
    var currentTimePromise;
    var duration = 0;
    mp3durations(filename).then(function (_duration) {
      duration = _duration;
    });

    // TODO suspicious b/c it will need audio resources
    var audio = new Media(
      'documents://'+filename,
      function () {
      },
      function (err) {
        $log.debug('we have problems: ' + err.message);
      },
      function (_status) {
        switch (_status) {
          case Media.MEDIA_NONE:
          case Media.MEDIA_PAUSED:
          case Media.MEDIA_STOPPED:
            pausedStatus = true;
            break;
          case Media.MEDIA_STARTING:
          case Media.MEDIA_RUNNING:
          default:
            pausedStatus = false;
            break;
        }
      }
    );

    // start timer to update currentTime (currentPosition) here
    (function updateCurrentTime() {
      if (audio && !pausedStatus) {
        audio.getCurrentPosition(function (_position) {
          currentTime = _position;
        }, function (err) {
          $log.debug('position: wat? '+err.message);
        });
      }
      currentTimePromise = $timeout(updateCurrentTime, 250);
    }());

    player.play = function () {
      audio.play();
    };
    player.pause = function () {
      audio.pause();
    };
    player.stop = function () {
      audio.stop();
      currentTime = 0;
    };
    Object.defineProperties(player, {
      "currentTime": {
        "get": function () {
          return currentTime; // audio.getCurrentPosition(); // audio.currentTime;
        },
        "set": function (_currentTime) {
          // Apparently seekTo sets time in milliseconds, whereas
          // getCurrentPosition gets time in seconds.
          audio.seekTo(1000*_currentTime); // audio.currentTime = currentTime;
        }
      },
      "duration": {
        "get": function () {
          return duration; // audio.getDuration(); // audio.duration;
        }
      },
      "paused": {
        "get": function () {
          return pausedStatus; // audio.paused;
        }
      },
      "allDone": {
        "get": function () {
          return pausedStatus && (currentTime >= duration)
        }
      }
    });

    player.cleanup = function () {
      audio.release(); // audio.src = '';
      audio = null;
      $timeout.cancel(currentTimePromise);
    };

    return player;
  };

  return service;
})
