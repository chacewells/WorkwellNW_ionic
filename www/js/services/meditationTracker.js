angular.module('workwellnw.services')
.factory('meditationTracker',
['$rootScope', 'practiceStatsManager',
function ($rootScope, practiceStatsManager) {
  var timeStarted = null,
  deregs = [];

  function handleBreak(guided) {
    var timeFinished = Date.now();
    var timeSpent = timeStarted && ((timeFinished || 0) - (timeStarted || 0)) || 0; // yay for JavaScript!
    (timeSpent && practiceStatsManager.addOne || angular.noop)(timeSpent, !!guided);
    timeStarted = timeFinished = null;
  }

  function meditationTracker() {
    meditationTracker.done();
    deregs.push($rootScope.$on('meditation:start', function (event) {
      timeStarted = Date.now();
    }));
    deregs.push($rootScope.$on('meditation:break', function (event, guided) {
      handleBreak(guided);
    }));
    deregs.push($rootScope.$on('meditation:clean', function (event) {
      timeStarted = null;
      practiceStatsManager.clear();
    }));
    deregs.push($rootScope.$on('meditation:done', function (event, guided) {
      handleBreak(guided);
      practiceStatsManager.flush();
    }));
  }

  // deregisters each callback and empties the deregs
  meditationTracker.done = function () {
    for (;deregs.length > 0; (deregs.pop() || angular.noop)());
  };

  return meditationTracker;
}])
