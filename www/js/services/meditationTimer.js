angular.module('workwellnw.services')

.factory('meditationTimer',
['$ionicPlatform', '$cordovaNativeAudio', '$log', '$interval', '$timeout', '$q', 'hmsFactory', '$window',
function ($ionicPlatform, $cordovaNativeAudio, $log, $interval, $timeout, $q, hmsFactory, $window) {
  var service = {};
  var running = false;
  var done = true;
  var firstTick = false;
  var _defaultPacer = 60 * 5; // 5 minutes
  var _pacer;
  var _soundId;
  var timeoutPromise;
  var intervalPromise;
  var isNativeAvailable = true;

  function id(sound) {
    return sound + '_playback_id';
  }

  function path(filename) {
    return 'media/' + filename + '.mp3';
  }

  service.withSound = function (sound) {
    var timer = {};
    var hms = hmsFactory(0, 0, 0);
    var soundPath = path(sound);
    _soundId = id(sound);
    try {
      $ionicPlatform.ready(function () {
        /*
         * TODO suspicious even though wrapped in ready; read ready() docs
         * & make sure i'm using this right
         */
        $cordovaNativeAudio.preloadSimple(_soundId, path(sound))
        .then(function (msg) {
          $log.debug(msg);
        }, function (err) {
          isNativeAvailable = false;
          $log.warn('Error: ' + err);
        });
      });
    } catch (e) {
      isNativeAvailable = false;
    }

    function play() {
      if (isNativeAvailable) {
        $log.debug('playing with nativeAudio');
        $cordovaNativeAudio.play(_soundId);
      } else {
        $log.debug('playing without nativeAudio');
        var audio = new Audio(soundPath);
        audio.play();
        $timeout(function () {
          audio.src = '';
        }, 2000);
      }
    }

    function playFinal() {
      play();
      intervalPromise = $interval(play, 1000, 2);
    }

    function tick () {
      $log.debug('ticking');
      $log.debug('_pacer is', _pacer);
      $log.debug('hms.getTime() returns', hms.getTime());
      timer.cb({hours: hms.hours, minutes: hms.minutes, seconds: hms.seconds});
      if (hms.getTime() > 0 && running) {
        timeoutPromise = $timeout(tick, 1000);
        if (firstTick) {
          firstTick = false;
        } else if (hms.getTime() % _pacer === 0) {
          play();
        }
        hms.subtractSeconds(1);
      } else if (hms.getTime() <= 0){
        playFinal();
        (timer.doneCb || angular.noop)();
        running = false;
        done = true;
      }
    }

    function flush() {
      $timeout.cancel(timeoutPromise);
      $interval.cancel(intervalPromise);
      running = false;
    }

    timer.setTime = function (config) {
      var hrs = Number(config.hours) || 0,
      min = Number(config.minutes) || 0,
      sec = Number(config.seconds) || 0;
      hms.setTime(sec).addMinutes(min).addHours(hrs);
      $log.debug(hrs,min,sec);
      $log.info(angular.toJson(hms));
      return this;
    };

    timer.whenDone = function (cb) {
      timer.doneCb = cb;
    };

    timer.start = function (cb, pacer) {
      $log.debug('timer starting');
      flush();
      _pacer = (typeof pacer === 'number') ? pacer : _defaultPacer;
      timer.cb = cb;
      running = true;
      firstTick = true;
      done = false;
      tick();
      return this;
    };

    timer.stop = function () {
      flush();
      done = true;
      hms.setTime(0);
      if (timer.cb) {
        timer.cb({hours: hms.hours, minutes: hms.minutes, seconds: hms.seconds});
      }
      return this;
    };

    timer.pause = function () {
      flush();
      return this;
    };

    timer.resume = function () {
      if (!running) {
        running = true;
        timeoutPromise = $timeout(tick, 1000);
      }
      return this;
    };

    timer.cleanup = function () {
      this.stop();
      // TODO suspicious b/c not wrapped in ready
      _soundId && $cordovaNativeAudio.unload(_soundId);
    }

    Object.defineProperty(timer, "running", {
      get: function () {
        return running;
      }
    });
    Object.defineProperty(timer, "done", {
      get: function () { return done; }
    });

    return timer;
  };

  return service;
}])
