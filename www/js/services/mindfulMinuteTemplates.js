angular.module('workwellnw.services')

.factory('mindfulMinuteTemplates',
['$log', 'jsonResourceManager', 'MINDFUL_MINUTE_SOUNDS', 'mediaUrl',
function ($log, jsonResourceManager, MINDFUL_MINUTE_SOUNDS, mediaUrl) {
    var messages;
    var service = {};
    jsonResourceManager.readResourceData('templates').then(function (storedMessages) {
        messages = storedMessages;
    }, function (errorText) {
        $log.error("jsonResourceManager response status text: " + errorText);
    });

    function randIdx (arr) {
      return Math.floor(Math.random() * arr.length);
    }

    function randItem(arr) {
      return arr[randIdx(arr)];
    }

    service.random = function () {
        return {
            title: 'Mindful Minute',
            message: randItem(messages),
            sound: mediaUrl.mp3( randItem(MINDFUL_MINUTE_SOUNDS) )
        };
    };

    return service;
}])
