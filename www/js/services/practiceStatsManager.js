angular.module('workwellnw.services')
.factory('practiceStatsManager',
['practiceStatsPersistence', 'dateUtil', 'wkSet',
function (practiceStatsPersistence, dateUtil, wkSet) {
  var practiceStats = practiceStatsPersistence.retrieve();
  var service = {};

  function last(arr) {
    var idx = arr.length - 1;
    if (idx > -1) {
      return arr[idx];
    }
    return null;
  }

  function getLast() {
    var lastStat = last(practiceStats),
    curr = dateUtil.currentWeekOf(),
    next = dateUtil.nextWeekOf();
    if (!lastStat || !dateUtil.is(lastStat.weekOf).between(curr, next)) {
      lastStat = newStatCurrent();
    }
    if (practiceStats[practiceStats.length - 1] !== lastStat) {
      practiceStats.push(lastStat);
    }
    return lastStat;
  }

  function newStatCurrent() {
    return {
      weekOf: dateUtil.currentWeekOf(),
      numPracticeSessions: 0,
      numGuided: 0,
      timePracticed: 0,
      daysPracticed: wkSet.EMPTY
    };
  }

  service.addOne = function (timeSpent, guided) {
    var lastStat = getLast();
    lastStat.timePracticed += timeSpent;
    ++lastStat.numPracticeSessions;
    lastStat.numGuided += Number(!!guided);
  };

  service.flush = function () {
    practiceStatsPersistence.save(practiceStats);
  };

  service.clear = function () {
    practiceStatsPersistence.clear();
    practiceStats = practiceStatsPersistence.retrieve();
  }

  return service;
}])
