angular.module('workwellnw.services')

.service('audioDownload',
['$q', '$cordovaFile', '$cordovaFileTransfer', 'mp3checksums',
function ($q, $cordovaFile, $cordovaFileTransfer, mp3checksums) {
  /**
   * usage (works like a curry):
   * var filename = 'some_audio.mp3';
   * var dl = audioDownload('www.someurl.com/some/path');
   * dl.downloadFile(filename, cordova.file.applicationDirectory+'www/media/'+filename)
   * .then(function (file) {
   * // succeed here
   * }, function (fail) {
   * // fail here
    });
   */
  return function (base) {
    var service = {};
    service.downloadFile = function (fileName, to) {
      var url = base;
      url += (url.charAt(url.length - 1) === '/') ? '' : '/';
      url += fileName;

      // asynch callback
      var deferred = $q.defer();

      // TODO suspect call to native API
      // download logic
      $cordovaFileTransfer.download(url, to, {}, true)
      .then(function (result) {
        resolveLocalFileSystemURL(to, function (fileEntry) {

          md5sum.file(fileEntry, function (checksum) {
            // if sum is good
            if ( mp3checksums()[fileName] === checksum ) {
              deferred.resolve(fileEntry);
            // if it's bad
            } else {
              deferred.reject('sum no good');
            }

          }, function (chksumErr) {
            deferred.reject(chksumErr);
          });
        });

      }, function (err) {
        deferred.reject(err);
      }, function (progress) {});

      return deferred.promise;
    };

    return service;
  };
}])
