angular.module('workwellnw.services')

.factory('jsonResourceManager', ['$http', function ($http) {
    function resourcePathForName(name) {
        return 'data/' + name + '.json';
    }

    function resourceFileName(name) {
        return name + '.json';
    }

    var service = {};

    // TODO suspect call to native API
    service.readResourceData = function (name) {
        return $http.get(resourcePathForName(name)).then(function (response) {
            return response.data;
        }, function (response) {
            return response.statusText;
        });
    };

    return service;
}])
