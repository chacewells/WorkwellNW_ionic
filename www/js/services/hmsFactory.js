angular.module('workwellnw.services')

.factory('hmsFactory', function () {
  function hmsFactory (h,m,s) {
    var seconds = (h*3600) + (m*60) + s;

    return {
      get hours () { return Math.floor(seconds / 3600); },
      get minutes () { return Math.floor( (seconds % 3600) / 60); },
      get seconds () { return seconds % 60; },
      addHours: function (hrs) {
        seconds += hrs * 3600;
        return this;
      },
      subtractHours: function (hrs) {
        return this.addHours(-hrs);
      },
      addMinutes: function (min) {
        seconds += min * 60;
        return this;
      },
      subtractMinutes: function (min) {
        return this.addMinutes(-min);
      },
      addSeconds: function (sec) {
        seconds += sec;
        return this;
      },
      subtractSeconds: function (sec) {
        return this.addSeconds(-sec);
      },
      getTime: function () {
        return seconds;
      },
      setTime: function (sec) {
        seconds = sec;
        return this;
      }
    };
  }

  return hmsFactory;
});
