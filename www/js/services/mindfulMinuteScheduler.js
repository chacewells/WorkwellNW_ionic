angular.module('workwellnw.services')

.factory('mindfulMinuteScheduler',
['$log', '$rootScope', '$cordovaLocalNotification', '$ionicPlatform', 'mindfulMinuteTemplates', 'mindfulMinuteUtil', 'dateUtil', 'LOCAL_NOTIFICATION_LIMIT', 'DayConstants', 'TimeConstants',
 function (
   $log,
   $rootScope,
   $cordovaLocalNotification, // maybe try not using ngCordova?
   $ionicPlatform,
   mindfulMinuteTemplates,
   mindfulMinuteUtil,
   dateUtil,
   LOCAL_NOTIFICATION_LIMIT,
   DayConstants,
   TimeConstants) {
    var service = {};

    function nextTimeOfDayConfig(date) {
        /* date's time of day today or tomorrow if today's has passed */
        return dateUtil.nextTimeOfDay(date, new Date());
    }

    function nextTimeOfDayWeekdayConfig(date) {
        /* date's time of day today or next weekday's if today's has passed or today falls on a weekend */
        return dateUtil.nextTimeOfDayWeekday(date, new Date());
    }

    /* Add one day, or return Monday if tomorrow is Saturday or Sunday */
    function nextWeekdayStrategy(date) {
        var newDate = new Date(date.getTime() + TimeConstants.DAY);

        if ( newDate.getDay() === DayConstants.SATURDAY ||
             newDate.getDay() === DayConstants.SUNDAY ) {
            newDate = dateUtil.nextMonday( newDate );
        }

        return newDate;
    }

    /* just a generator. starts at 0. returns last value + 1 each time */
    function idGenerator() {
        var next = 0;
        return function () {
            return next++;
        };
    }

    /**
     * Builds a function that generates a week's worth of mindful minute instances that will be used to schedule local notifications.
     * @param mm            A mindful minute containing the time of day to schedule.
     * @param first         The first date (used as the sentinel to determine if we've reached a week).
     * @param qualifyDate   A predicate taking a date, returning true or false. Determines whether an instance is built on a particular day.
     * @param nextId        A generator from which the next id is picked.
     * return               An array of mindful minute instances for a week (less those filtered out by qualifyDate).
     */
    function makeAWeek(mm, first, nextId) {
        return function _makeAWeek(mmArray, date) {
            var instance, scheduleMm;
            var next = new Date(date.getTime() + TimeConstants.DAY);

            scheduleMm = angular.copy(mm);
            scheduleMm.timeOfDay = date;
            instance = mindfulMinuteUtil.toInstance(scheduleMm, mindfulMinuteTemplates.random());
            instance.id = nextId();
            instance.every = 'week';

            return ( next.getDay() === first.getDay() )
                ? mmArray.concat(instance ? [instance] : [])
                : _makeAWeek( mmArray.concat(instance ? [instance] : []), next );
        };
    }

    function toInstances(initializeDate, nextId) {
        return function (mm) {
            var first = initializeDate(mm.timeOfDay);
            return makeAWeek(mm, first, nextId)( [], first );
        };
    }

    function makeInstances(mindfulMinutes, initializeDate, qualifyDate) {
        'use strict';
        var nextId = idGenerator();

        var instances = mindfulMinutes
                            .map( toInstances(initializeDate, nextId) )
                            .reduce(function (a, b) { return a.concat(b) }, [])
                            .filter(qualifyDate)
                            .sort(function (a, b) {
                                return a.at.getTime() - b.at.getTime();
                            });

        return instances;
    }

    /**
     * Repeats each mindfulMinute.timeOfDay daily over numDays and returns the
     * resulting array. Configurable with initializeDate and nextDateFn.
     * initializeDate is used to determine--from a given date--which day
     * on which to create the first MindfulMinuteInstance. nextDateFn is used
     * to determine how each subsequent date should be rolled from its
     * predecessor.
     * @param mindfulMinutes The MindfulMinutes to create instances of.
     * @param numDays How many MindfulMinuteInstances to create.
     * @param initializeDate A Date -> Date function that determines the first
     * date in the MindfulMinuteInstances.
     * @param nextDateFn A Date -> Date function that determines each
     * subsequent instance's date from the last.
     * @returns An array of numDays MindfulMinuteInstances containing
     * (numDays / mindfulMinutes.length) instances for each MindfulMinute.
     */
     $ionicPlatform.ready(function () {
      /**
       * Configurable mindful minute scheduler.
       * @param mindfulMinutes The MindfulMinute instances to schedule.
       * @param scheduleConfigFn A Date -> Date function. This is used to
       * initialize the first mindful minute to be scheduled.
       * @param nextDateFn A Date -> Date function. This is used to configure how
       * each subsequent mindful minute is determined from its predecessor.
       * @returns Nothing.
       */
      service.scheduleOutCustom = function (mindfulMinutes, scheduleConfigFn, filterMm) {
          var numDays = Math.floor(LOCAL_NOTIFICATION_LIMIT / mindfulMinutes.length); // TODO maybe this should go away
          
          var instances = makeInstances(mindfulMinutes, scheduleConfigFn, filterMm);
          // this.toInstances(mindfulMinutes, numDays, scheduleConfigFn, nextDateFn);
          try {
              $cordovaLocalNotification.schedule(instances);
          } catch (e) {
              //Probably debugging in a web browser. Let's just log the error and move on...
              $log.debug(e.name + ': ' + e.message + ' [probably just browser debugging]');
          }
      };

      /**
       * The standard mindful minute scheduler. It expects an array of mindful
       * minutes and schedules them according to a weekday strategy. The first
       * date is determined from the time of day contained in each
       * mindfulMinute.timeOfDay. The time of day today is used if it has not
       * already passed. Otherwise, the time of day on the next weekday is used.
       * @param mindfulMinutes An array of MindfulMinute objects.
       */
      service.scheduleOut = function (mindfulMinutes) {
        this.scheduleOutCustom(
            mindfulMinutes,
            nextTimeOfDayConfig,
            function (mm) {
                var day = mm.at.getDay();
                return !(day === DayConstants.SATURDAY || day === DayConstants.SUNDAY);
            });
      };

      /**
       * Syntactic sugar for scheduleOut(). If the argument passed is a single
       * Object, it is wrapped in an array and passed on to scheduleOut().
       * If it is an array, it is simply passed onto scheduleOut(). No validation
       * beyond single vs. plural happens, however, so the caller is assumed
       * to know what it's doing (since it is likely me :). If toSchedule() is
       * neither an array or a plain object, nothing happens.
       * @param toSchedule The MindfulMinute or array of MindfulMinutes.
       */
      service.schedule = function (toSchedule) {
        // TODO suspect call to native API
          $cordovaLocalNotification.cancelAll();
          // cordova.plugins.notification.local.cancelAll();
          if (toSchedule.constructor == Object) {
              service.scheduleOut([toSchedule]);
          } else if (toSchedule.constructor == Array) {
              service.scheduleOut(toSchedule);
          }
      };
    });

    return service;
}]);
