angular.module('workwellnw.services')
.factory('helptipState', function (persistenceManager) {
  var STORAGE_KEY = 'helptips';
  // TODO suspicious b/c persistenceManager relies on localStorage, an HTML5 API
  var helptips = persistenceManager.retrieve(STORAGE_KEY) || {};

  function wasShown(helptipKey) {
    var tipWasShown = false;
    if (helptipKey in helptips) {
      tipWasShown = helptips[helptipKey];
    } else {
      helptips[helptipKey] = tipWasShown; // false, duh
      persistenceManager.persist(STORAGE_KEY, helptips);
    }

    return tipWasShown;
  }

  function setShown(helptipKey) {
    helptips[helptipKey] = true;
    persistenceManager.persist(STORAGE_KEY, helptips);
  }

  function setUnshown(helptipKey) {
    helptips[helptipKey] = false;
    persistenceManager.persist(STORAGE_KEY, helptips);
  }

  function unset(helptipKey) {
    delete helptips[helptipKey];
    persistenceManager.persist(STORAGE_KEY, helptips);
  }

  function clear() {
    helptips = {};
    persistenceManager.persist(STORAGE_KEY, helptips);
  }

  return {
    wasShown: wasShown,
    setShown: setShown,
    setUnshown: setUnshown,
    unset: unset,
    clear: clear
  };
})
