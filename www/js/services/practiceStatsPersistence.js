angular.module('workwellnw.services')

.factory('practiceStatsPersistence',
['MEDITATION_PRACTICES_KEY', 'persistenceManager',
function (MEDITATION_PRACTICES_KEY, persistenceManager) {
  var practiceStatsProperties = {
    'weekOf': 'Date',
    'numPracticeSessions': 'number',
    'numGuided': 'number',
    'timePracticed': 'number'
  };
  var service = {};

  service.retrieve = function () {
    var practiceStats = persistenceManager.retrieve(MEDITATION_PRACTICES_KEY) || [];

    angular.forEach(practiceStats, function (practiceStat, idx) {
      var weekOf = new Date(practiceStat.weekOf);
      practiceStats[idx] = {
        weekOf: weekOf,
        numPracticeSessions: practiceStat.numPracticeSessions,
        numGuided: practiceStat.numGuided,
        timePracticed: practiceStat.timePracticed
      };
    });

    return practiceStats;
  };

  service.save = function (practiceStats) {
    persistenceManager.persist(MEDITATION_PRACTICES_KEY, practiceStats);
  };

  service.clear = function () {
    persistenceManager.persist(MEDITATION_PRACTICES_KEY, []);
  };

  return service;
}])
