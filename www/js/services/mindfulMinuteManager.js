angular.module('workwellnw.services')

.factory('mindfulMinuteManager',
function (
  mindfulMinutePersistence,
  mindfulMinuteScheduler,
  $log) {
    var service = {};

    service.newDate = function () {
        var d = new Date();
        d.setSeconds(0, 0);
        return d;
    };

    service.create = function () {
        return {
            timeOfDay: service.newDate()
        };
    };

    service.all = function () {
        return angular.copy(mindfulMinutePersistence.retrieve());
    };

    service.update = function (mindfulMinutes) {
        mindfulMinutePersistence.save(mindfulMinutes);
        mindfulMinuteScheduler.schedule(mindfulMinutes);
    };

    return service;
});
