angular.module('workwellnw.services')

.factory('mindfulMinuteUtil', [function () {
    var service = {};

    /**
     * Merges a registered mindful minute with a scheduling template.
     * @param mindfulMinute The registered mindful minute.
     * @param template The scheduling template.
     * @return The resulting merge object (mindful minute instance).
     */
    service.toInstance = function (mindfulMinute, template) {
        return {
            at: new Date(mindfulMinute.timeOfDay.getTime()),
            title: template.title,
            text: template.message,
            sound: template.sound
        };
    };

    /**
     * Given an array, replaces a mindful minute with another.
     * @param value The mindfulMinute to be replaced.
     * @param replacement The replacement.
     * @return The element that was removed by Array#splice(), or
     * a new mindful minute.
     */
    service.replace = function (anArray, value, replacement) {
        var index = anArray.indexOf(value);
        if (index > -1 &&
            replacement &&
            'timeOfDay' in replacement) {
            var result = anArray.splice(index, 1, replacement)[0] || {
                timeOfDay: new Date(1970, 1, 1)
            };
        }
        return result;
    };

    return service;
}])
