angular.module('workwellnw.services')

.factory('wkSet', function () {
  var service = {
      EMPTY       : 0x0,
      SUNDAY      : 0x1,
      MONDAY      : 0x2,
      TUESDAY     : 0x4,
      WEDNESDAY   : 0x8,
      THURSDAY    : 0x10,
      FRIDAY      : 0x20,
      SATURDAY    : 0x40,
      ALL         : 0x7f
    };

  service.put = function (set, day) {
    var result = set;
    if (day <= this.ALL) {
      result = set|day;
    }
    return result;
  }

  service.putAt = function (set, dayNum) {
    if (dayNum > 6 || dayNum < 0) {
      return set;
    }
    var shifted = this.SUNDAY << dayNum;
    return set|shifted;
  }

  service.count = function (v) {
    var c = (v & 0x55555555) + ((v >> 1) & 0x55555555);
    c = (c & 0x33333333) + ((c >> 2) & 0x33333333);
    c = (c & 0x0F0F0F0F) + ((c >> 4) & 0x0F0F0F0F);
    c = (c & 0x00FF00FF) + ((c >> 8) & 0x00FF00FF);
    c = (c & 0x0000FFFF) + ((c >> 16)& 0x0000FFFF);
    return c;
  }

  service.drop = function (set, day) {
    if (day > this.ALL || day < 0x0) {
      return set;
    }
    return set^day;
  }

  service.contains = function (set, val) {
    return (set&val) !== 0;
  }

  return service;
})
