angular.module('workwellnw.services')

.service('mp3checksums',
['$cordovaFile', '$log', '$ionicPlatform',
function ($cordovaFile, $log, $ionicPlatform) {
    var _mp3checksums_cache;
    /* TODO definitely suspicious since this is invoked immediately as the
    * dependent service loads
    */
    // $ionicPlatform.ready(function () {
      $cordovaFile.readAsText(cordova.file.applicationDirectory, 'www/media/checksums.json')
      .then(function (text) {
          $log.debug(text);
          _mp3checksums_cache = angular.fromJson(text);
      }, function (err) {
          $log.debug("no file here");
      });
    // });

    return function () {
      return angular.copy(_mp3checksums_cache);
    };
}])
