angular.module('workwellnw.services')

.factory('mp3durations',
['$cordovaFile', '$log', '$q',
function ($cordovaFile, $log, $q) {
  var _durations_cache;

  // i don't ever plan to call this without _durations_cache being initialized
  // so i'm not protecting against it
  function resolveDuration(deferred, filename) {
    if (typeof _durations_cache[filename] !== 'undefined' && _durations_cache[filename]) {
      deferred.resolve(_durations_cache[filename]);
    } else {
      deferred.reject("Can't find duration for '"+filename+"'");
    }
  }

  return function (filename) {
    var deferred = $q.defer();

    if (_durations_cache) {
      deferred.resolve(_durations_cache[filename]);
    } else {
      /* TODO suspicous although not called until client asks */
      $cordovaFile.readAsText(cordova.file.applicationDirectory, 'www/media/durations.json')
      .then(function (text) {
        _durations_cache = angular.fromJson(text);
        resolveDuration(deferred, filename);
      }, function (err) {
        deferred.reject(err);
      });
    }

    return deferred.promise;
  };
}])
