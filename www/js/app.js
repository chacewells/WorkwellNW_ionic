angular.module('workwellnw', ['ionic', 'workwellnw.controllers', 'workwellnw.services', 'workwellnw.filters', 'workwellnw.constants', 'workwellnw.directives'])

.run(
function (
  mindfulMinutePersistence,
  mindfulMinuteScheduler,
  $ionicPlatform,
  $rootScope,
  $ionicPopup,
  $log,
  meditationTracker) {

  $ionicPlatform.ready(function () {
    meditationTracker(); // register the meditation tracker event handlers
    $log.log('this is log');
  });

  /**
   * This displays the Mindful Minute message text when the user clicks
   * a local notification pop up.
   */
  $ionicPlatform.ready(function () {
    $rootScope.$on('$cordovaLocalNotification:click', function (event, notification, state) {
      $log.debug('local notification clicked here');

      $ionicPopup.show({
        title: notification.title,
        template: notification.text,
        buttons: [
          {
            text: 'Okay',
            type: 'button-positive'
          }
        ]
      }).then(function (res) {
        $log.debug("That ended quickly!");
      });

    });
  });

    /* Schedule out stored mindful minutes so it seems like they appear to
       repeat forever (really they'll each repeat 60 / (# instances) times
       if the app is never opened again)
     */
    $ionicPlatform.on('resume', function () {
      $ionicPlatform.ready(function () {
        mindfulMinuteScheduler.schedule(mindfulMinutePersistence.retrieve());
      });
    });

    /* logging to see if notifications are actually being scheduled */
    $rootScope.$on("$cordovaLocalNotification:schedule", function(event, notification, state) {
      $log.debug( "scheduled: (" + notification.id + ") - " + new Date(notification.at * 1000).toString() );
    });
})

.config(function ($stateProvider, $urlRouterProvider, $logProvider) {
    $logProvider.debugEnabled(true);

    $urlRouterProvider.otherwise('/tabs/mindfulminutes');

    $stateProvider
    .state('tabs', {
      url: '/tabs',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })
    .state('tabs.mindfulminutes', {
        url: '/mindfulminutes',
        views: {
          'mindfulminutes-tab': {
            templateUrl: 'templates/mindful-minute-list.html',
            controller: 'MindfulMinuteListController'
          }
        }
    })
    .state('tabs.meditationtimer', {
        url: '/meditationtimer',
        views: {
          'meditationtimer-tab': {
            templateUrl: 'templates/meditation-timer.html',
            controller: 'MeditationTimerController'
          }
        }
    })
    .state('tabs.guidedmeditationList', {
        url: '/guidedmeditation-list',
        views: {
          'guidedmeditation-tab': {
            templateUrl: 'templates/guided-meditation-list.html',
            controller: 'GuidedMeditationListController'
          }
        }
    })
    .state('tabs.guidedmeditationPlayer', {
      url: '/guidedmeditation-player/:id',
      views: {
        'guidedmeditation-tab': {
          templateUrl: 'templates/guided-meditation-player.html',
          controller: 'GuidedMeditationPlayerController'
        }
      }
    })
    .state('tabs.stats', {
        url: '/stats',
        views: {
          'stats': {
            templateUrl: 'templates/stats.html',
            controller: 'StatsController'
          }
        }
    })
});
