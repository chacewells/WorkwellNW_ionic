angular.module('workwellnw.constants', [])

.constant('MINDFUL_MINUTES_KEY', 'mindfulMinutesKey')

.constant('MEDITATION_PRACTICES_KEY', 'meditationPracticesKey')

.constant('LOCAL_NOTIFICATION_LIMIT', 63)

.constant('MINDFUL_MINUTE_SOUNDS', ['klang'])

.constant('BellUnit', { SECONDS: 1, MINUTES: 60 })

.constant('GuidedMeditations',
[
    {
        id: 'body_scan',
        title: 'Body Scan',
        filename: 'Body%20Scan%20-%2045%20minutes.mp3',
        localFile: 'body_scan.mp3'
    },
    {
        id: 'driving',
        title: 'Driving',
        filename: 'driving%20meditation.mp3',
        localFile: 'driving_meditation.mp3'
    },
    {
        id: 'intro_to_body_scan',
        title: 'Intro to Body Scan',
        filename: 'Intro%20to%20body%20scan.mp3',
        localFile: 'intro_to_body_scan.mp3'
    },
    {
        id: 'long_sitting',
        title: 'Long Sitting',
        filename: 'Long%20Sitting%20Meditation.mp3',
        localFile: 'long_sitting_meditation.mp3'
    },
    {
        id: 'lovingkindness',
        title: 'Loving Kindness',
        filename: 'Lovingkindess%20Meditation.mp3',
        localFile: 'lovingkindness_meditation.mp3'
    },
    {
        id: 'short_sitting',
        title: 'Short Sitting',
        filename: 'Short%20sitting%20meditation.mp3',
        localFile: 'short_sitting_meditation.mp3'
    },
    {
        id: 'yoga_nidra',
        title: 'Yoga Nidra',
        filename: 'Yoga%20Nidra.mp3',
        localFile: 'yoga_nidra.mp3'
    }
])

.constant('GUIDED_MEDITATION_DOWNLOAD_URL', 'http://threetreesyoga.com/MBRT/mp3files/')

.constant('DayConstants', {
    SUNDAY : 0,
    MONDAY : 1,
    TUESDAY : 2,
    WEDNESDAY : 3,
    THURSDAY : 4,
    FRIDAY : 5,
    SATURDAY : 6
})

.constant('TimeConstants', {
    WEEK : 3600 * 24 * 7 * 1000,
    DAY  : 3600 * 24 * 1000,
    HOUR : 3600 * 1000,
    MINUTE : 60 * 1000,
    SECOND : 1000
})
;
