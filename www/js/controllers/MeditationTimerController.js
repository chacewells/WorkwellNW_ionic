angular.module('workwellnw.controllers')

.controller('MeditationTimerController',
function (
  $scope,
  $ionicPopup,
  meditationTimer,
  BellUnit,
  $timeout,
  helptipState) {
  var timer;
  var updatingRunning; // keeps callback to cancel updateRunning()
  var timerHelptipKey = 'meditationtimer';
  init();

  function init() { // initialize the model immediately
    timer = meditationTimer.withSound('ping');
    timer.whenDone(function () {
      $scope.$emit('meditation:done', false);
    });
    $scope.BellUnit = BellUnit; // enum for "Units" config in view
    $scope.config = {
      hours: 0,
      minutes: 0,
      seconds: 0,
      bellInterval: 10,
      bellUnit: BellUnit.MINUTES
    };
    $scope.timeRemaining = {
      hours: 0,
      minutes: 0,
      seconds: 0
    };
    updatingRunning || updateRunning();

    if ( !helptipState.wasShown(timerHelptipKey) ) {
      showHelptip();
    }
  }

  function showHelptip() {
    $ionicPopup.show({
      title: 'Mindful Minute Help',
      template: "The meditation timer paces your own meditations.<br>"
              + "Hours, minutes and seconds sets the timer duration.<br>"
              + "An interval will play periodically to help you pace yourself."
              + " Choose an interval in minutes or seconds.",
      buttons: [
        {
          text: 'Got it!',
          type: 'button-positive',
          onTap: function () {
            helptipState.setShown(timerHelptipKey);
          }
        }
      ]
    })
  }

  function updateRunning() { // update the running boolean every so often
    $scope.running = timer.running;
    updatingRunning = $timeout(updateRunning, 250);
  }

  $scope.getDuration = function () {
    return ($scope.timeRemaining.hours * 3600)
      + ($scope.timeRemaining.minutes * 60)
      + ($scope.timeRemaining.seconds);
  };

  $scope.start = function () {
    if (timer.running) {
      $scope.pause();
    } else if (!timer.done) {
      $scope.resume();
    } else {
      var pacer = $scope.config.bellInterval * $scope.config.bellUnit;
      $scope.$emit('meditation:start');
      timer.setTime($scope.config);
      timer.start(function (remaining) {
        $scope.timeRemaining = remaining;
      }, pacer);
    }
  };

  $scope.stop = function () {
    $scope.$emit('meditation:break', false);
    timer.stop();
  };

  $scope.resume = function () {
    $scope.$emit('meditation:start');
    timer.resume();
  };

  $scope.pause = function () {
    $scope.$emit('meditation:break', false);
    timer.pause();
  };

  // events
  $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if (fromState.name === 'tabs.meditationtimer') {
      $scope.$emit('meditation:done', false);
      $timeout.cancel(updatingRunning);
      updatingRunning = null;
    } else if (toState.name === 'tabs.meditationtimer') {
      updatingRunning || updateRunning();

      if ( !helptipState.wasShown(timerHelptipKey) ) {
        showHelptip();
      }
    }
  });

})
