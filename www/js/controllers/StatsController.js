angular.module('workwellnw.controllers')

.controller('StatsController',
function (
    $scope,
    $cordovaFile,
    $log,
    practiceStatsPersistence,
    helptipState,
    GuidedMeditations) {
    var stats;
    $scope.stats = getSummary();

    $scope.clearStats = function () {
        $scope.$emit('meditation:clean');
        $scope.stats = getSummary();
    };

    $scope.resetHelptips = function () {
        helptipState.clear();
        $scope.$emit('helptips:cleared');
    };

    /* TODO suspicious
      * declared before platform ready
      * altough probably never used until after
      */
    $scope.removeGuidedMeditations = function () {
        angular.forEach(GuidedMeditations, function (meditation, idx) {
            $cordovaFile.removeFile(cordova.file.documentsDirectory, meditation.localFile)
            .then(function (success) {
                $log.debug("Successfully removed '"+success.fileRemoved.nativeURL+"'");
            }, function (err) {
                $log.debug("'"+meditation.localFile+"' not present. Moving on...");
            })
        });
    };

    function getSummary() {
        stats = practiceStatsPersistence.retrieve();
        var summary = {};
        var numWeeks = stats.length;
        function getOrZero(obj, key) {
            var val = obj[key];
            return (typeof val === 'number') ? val : 0;
        }
        var totals = (function () {
            var result = {
                numPracticeSessions: 0,
                numGuided: 0,
                timePracticed: 0,
                numWeeks: stats.length
            };
            for (var i = 0; i < stats.length; ++i) {
                result.numPracticeSessions += getOrZero(stats[i], 'numPracticeSessions');
                result.numGuided += getOrZero(stats[i], 'numGuided');
                result.timePracticed += getOrZero(stats[i], 'timePracticed');
            }
            return result;
        }());

        summary.practiceSessionsPerWeek = (totals.numPracticeSessions / totals.numWeeks) || 0;
        summary.timePracticedPerWeek = (totals.timePracticed / totals.numWeeks) || 0;
        summary.numWeeks = totals.numWeeks || 0;
        return summary;
    }

    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        if (toState.name === 'tabs.stats') {
            stats = practiceStatsPersistence.retrieve();
        }
        $scope.stats = getSummary();
    });
});
