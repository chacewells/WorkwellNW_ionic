angular.module('workwellnw.controllers')

.controller('MindfulMinuteListController',
function (
  $log,
  $scope,
  mindfulMinuteManager,
  mindfulMinuteUtil,
  $window,
  $cordovaDatePicker,
  $ionicPopup,
  helptipState) {
    var mmHelptipKey = 'mindfulminute';
    var mindfulMinutes = mindfulMinuteManager.all();
    $scope.mindfulMinutes = mindfulMinutes;
    $scope.shouldShowDelete = false;
    init();

    function init() {
      if (!helptipState.wasShown(mmHelptipKey)) {
        showHelptip();
      }
    }

    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      if (toState.name === 'tabs.mindfulminutes') {
        init();
      }
    })

    $scope.timeExpr = function (mm) {
        var tod = mm.timeOfDay;
        return (tod.getHours() * 3600) + (tod.getMinutes() * 60);
    }

    function showHelptip() {
      $ionicPopup.show({
        title: 'Mindful Minute Help',
        template: "Welcome. A mindful minute reminds you"
                + " each weekday to slow down and be mindful.<br>"
                + "To schedule, tap the add (+) button"
                + " and select a time of day.<br>"
                + "To delete, tap the delete (-) button on the top left,"
                + " then tap the delete (-) button next to the"
                + " mindful minute.<br>"
                + "To update one, just tap it and pick a new time.",
        buttons: [
          {
            text: 'Got it!',
            type: 'button-positive',
            onTap: function () {
              helptipState.setShown(mmHelptipKey);
            }
          }
        ]
      })
    }

    // $scope methods
    $scope.removeMindfulMinute = function (mindfulMinute) {
        var idx = mindfulMinutes.indexOf(mindfulMinute);
        var deletedElements = mindfulMinutes.splice(idx, 1);
        mindfulMinuteManager.update(mindfulMinutes);
        $log.debug('mindful minute removed at time: ' + mindfulMinute.timeOfDay.toString());
    };

    $scope.toggleShowDelete = function () {
        $scope.shouldShowDelete = !$scope.shouldShowDelete;
    };

    $scope.mmcancel = function (swapOptions) {
        mindfulMinuteUtil.replace(mindfulMinutes, swapOptions.edited, swapOptions.original);
        doneEditing();
    };

    $scope.edit = function (toEdit) {
        $scope.saveAction = 'mmupdate';
        $scope.editing = toEdit;
        $scope.original = angular.copy(toEdit);
        editDate();
    };

    // local functions
    function editDate() {
      /* TODO suspicious
       * declared before platform ready
       * although probably not used until after
       */
      $cordovaDatePicker.show({
        date: $scope.editing.timeOfDay,
        mode: 'time'
      }).then(function (date) {
        $scope.editing.timeOfDay = date;
        $scope.$emit($scope.saveAction, $scope.editing);
      });
    }

    function doneEditing() { // clean up edit state
        $scope.editing = {};
        $scope.saveAction = null;
    }

    // events
    $scope.newMindfulMinute = function () {
        $scope.saveAction = 'mmsave';
        $scope.editing = mindfulMinuteManager.create();
        editDate();
    };

    $scope.$on('mmupdate', function (event, updated) {
      if (updated &&
          updated.timeOfDay &&
          updated.timeOfDay.constructor === Date) {
            mindfulMinuteManager.update(mindfulMinutes);
            $log.debug('mindful minutes updated: ' + angular.toJson(mindfulMinutes));
      } else {
        $scope.mmcancel({
          edited: updated,
          original: $scope.original
        });
      }
        doneEditing();
    });

    $scope.$on('mmsave', function (event, edited) {
      if (typeof edited !== 'undefined' &&
          typeof edited.timeOfDay !== 'undefined' &&
          edited.timeOfDay.constructor === Date) {
        mindfulMinutes.push(edited);
        mindfulMinuteManager.update(mindfulMinutes);
      }
        doneEditing();
    });
})
