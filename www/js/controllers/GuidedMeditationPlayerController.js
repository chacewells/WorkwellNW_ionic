angular.module('workwellnw.controllers')

.controller('GuidedMeditationPlayerController',
function (
  $scope,
  $log,
  $cordovaFile,
  $ionicPopup,
  $ionicHistory,
  $ionicLoading,
  $timeout,
  guidedMeditationPlayer,
  GuidedMeditations,
  GUIDED_MEDITATION_DOWNLOAD_URL,
  mp3checksums) {
  var player = null;
  var updatingRunning;
  var DOWNLOAD_PROGRESS_TEMPLATE = "<p>Downloading {{title}}...</p>\n"
                           + "<p>Progress: {{download.progress}}</p>\n"
                           + '<p><button ng-click="cancelDownload()" class="button button-clear button-positive">Cancel</button></p>';
  var fileTransfer = new FileTransfer();

  (function init() {
    $scope.data = {
     progress: 0,
     duration: 0
    };
    $scope.download = {
      progress: '0%'
    };

    updateRunning();
  }());

  function watchProgress() {
    if (!player) {
      return;
    }
    if (player.allDone) {
      player.currentTime = 0;
    }
    $scope.data.progress = player.currentTime;
    $log.debug('$scope.data.progress: '+$scope.data.progress);
    if (!$scope.data.duration) {
      $scope.data.duration = player.duration || 0;
      $log.debug('$scope.data.duration: '+$scope.data.duration);
    }
    $timeout(watchProgress, 1000);
  }

  function updateRunning() {
    $scope.running = (!player || player.paused) ? false : true;
    updatingRunning = $timeout(updateRunning, 250);
  }

  function _play() {
    player.play();
    $scope.$emit('meditation:start');
  }

  function _pause() {
      player.pause();
      $scope.data.progress = player.currentTime;
      $scope.$emit('meditation:break', true);
  }

  function initializePlayer() {
    player = guidedMeditationPlayer.newPlayer($scope.currentMeditation.localFile);
    player.play();
    watchProgress();
    $scope.$emit('meditation:start');
  }

  function answeredYesToDownload(event) {
    var currentMeditation = $scope.currentMeditation;
    var filename = currentMeditation.filename;
    var localFile = currentMeditation.localFile;
    var downloadTo = cordova.file.documentsDirectory + localFile;
    var isDownloadInProgress = false;

    $log.debug('downloading to ' + downloadTo);

    fileTransfer.onprogress =
    function (progress) {
      var percentProgress = String(progress.loaded / progress.total * 100);
      var endSlice = percentProgress.indexOf('.') + 3;
      percentProgress = percentProgress.slice(0, endSlice > 1 ? endSlice : undefined);
      $scope.download.progress = percentProgress+'%';
      if (!isDownloadInProgress) {
        isDownloadInProgress = true;
        $scope.cancelDownload = function () {
          fileTransfer.abort();
        };
        $ionicLoading.show({
          template : DOWNLOAD_PROGRESS_TEMPLATE,
          scope : $scope
        });
      }
    };

    fileTransfer.download(
      GUIDED_MEDITATION_DOWNLOAD_URL + filename,
      downloadTo
    // ).then(
      , function (fileEntry) {
      $ionicLoading.hide(); // this is success, so just hide the progress indicator
      md5chksum.file(fileEntry, function (checksum) {
        var realChecksum = mp3checksums()[localFile];
        if (realChecksum === checksum) {
          $log.debug("that's a good checksum");
          $scope.$emit('guidedmeditation:added');
          initializePlayer();
        } else {
          $log.debug("that checksum's no good. deleting file");
          /* TODO suspicious
           * declared before platform ready
           * although probably never called until platform ready
           */
          $cordovaFile.removeFile(
            cordova.file.documentsDirectory,
            localFile)
          .then(function (success) {
            var popupConfig = {
              title: $scope.title+': Security Issue',
              template: "The media file could not be verified. It "
                      + "has been discarded for your security.",
              buttons: [
                {
                  text: 'Okay',
                  type: 'button-assertive',
                  onTap: function () {
                    $ionicHistory.goBack();
                  }
                }
              ]
            };

            $log.debug("evil file deleted");
            $ionicPopup.show(popupConfig)
          }, function (err) {
            $log.debug("couldn't delete file, but i'm not doing anything about it");
            $ionicPopup.show(popupConfig);
          });
        }
      });

      isDownloadInProgress = false;
      $scope.download.progress = '0%';
    }, function (err) {
      $ionicLoading.hide();
      var errTitle, errTemplate;
      if (err.code == FileTransferError.ABORT_ERR) {
        errTitle = $scope.title+' Meditation: Download Cancelled';
        errTemplate = null;
      } else {
        errTitle = $scope.title+' Meditation: Download Failed';
        errTemplate = '<b>'+$scope.title+'</b> failed to download.'
        + ' Please try again later.';
      }
      $ionicPopup.show({
        title: errTitle,
        template: errTemplate,
        buttons: [
          {
            text: 'Okay',
            type: 'button-assertive',
            onTap: function () {
              $ionicHistory.goBack();
            }
          }
        ]
      });
    }, false);
  }

  function answeredNoToDownload(event) {
    $log.debug($scope.title+': No thanks, not right now');
    $ionicHistory.goBack();
  }

  function guidedMeditationFileNotFound(error) {
    var currentMeditation = $scope.currentMeditation;
    $ionicPopup.show({
      title: $scope.title,
      template: '<b>'+$scope.title+'</b> is not in your library.'
              + ' Would you like to download it now?',
      buttons: [
        {
          text: 'Yes',
          type: 'button-positive',
          onTap: answeredYesToDownload
        }, {
          text: 'No',
          type: 'button-calm',
          onTap: answeredNoToDownload
        }
      ]
    }).then(function (res) {
      $log.debug("event's over. missed your chance");
    });
  }

  $scope.updateProgress = function () {
    if (player) {
      player.currentTime = $scope.data.progress;
    }
  };

  $scope.play = function () {
    if (player) {
      player.paused ? _play() : _pause();
    }
  };

  $scope.stop = function () {
    player && player.stop();
    $scope.data.progress = player.currentTime;
    $scope.$emit('meditation:break', true);
  };

  $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    if (toState.name === 'tabs.guidedmeditationPlayer' && !player) {
      var currentMeditation =
      GuidedMeditations.find(function (gm) {
          return gm.id === toParams.id;
        });
      $scope.currentMeditation = currentMeditation;
      $scope.title = currentMeditation.title;
      /* TODO suspicious
       * declared before platform ready
       * but probably never called until ready
       */
      $cordovaFile.checkFile(cordova.file.documentsDirectory, currentMeditation.localFile)
      .then(initializePlayer, guidedMeditationFileNotFound); // initiates download dialog if not found
    }
  });

  $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if (fromState.name === 'tabs.guidedmeditationPlayer') {
      if (player) {
        player.cleanup();
        player = null;
      }
      $timeout.cancel(updatingRunning);
      $scope.$emit('meditation:done', true);
    }
  });
})
