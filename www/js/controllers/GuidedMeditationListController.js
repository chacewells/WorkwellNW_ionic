angular.module('workwellnw.controllers')

.controller('GuidedMeditationListController',
function (
  $scope,
  $rootScope,
  $ionicPopup,
  $cordovaFile,
  $log,
  GuidedMeditations,
  helptipState) {
  $scope.guidedMeditations = GuidedMeditations;
  $scope.shouldShowDelete = false;
  var gmHelptipKey = 'guidedmeditation';
  var Class = {
    HERE : 'gm-here',
    NOT_HERE : 'gm-not-here'
  };

  colorMeditations();

  $scope.toggleShowDelete = function () {
    $scope.shouldShowDelete = !$scope.shouldShowDelete;
  };

  $scope.removeMeditation = function (meditation) {
    /* TODO suspicous
     * declared before platform ready
     */
    $cordovaFile.removeFile(
      cordova.file.documentsDirectory,
      meditation.localFile)
    .then(function (success) {
      $ionicPopup.show({
        title: "Guided Meditation Deleted",
        template: meditation.title+" Meditation has been removed from your library.",
        buttons: [
          {
            text: 'Okay',
            type: 'button-positive',
            onTap: function () {
              $scope.$emit('guidedmeditation:removed');
            }
          }
        ]
      })
    }, function (err) {
      $log.debug("Nothing to delete");
    });
  };

  function colorMeditations() {
    angular.forEach($scope.guidedMeditations, function (meditation, idx) {
      /* TODO suspicious
       * declared before platform ready
       */
      $cordovaFile.checkFile(cordova.file.documentsDirectory, meditation.localFile)
      .then(function (file) {
        meditation.cssClass = Class.HERE;
      }, function (noFile) {
        meditation.cssClass = Class.NOT_HERE;
      });
    });
  }

  function showHelptip() {
    $ionicPopup.show({
      title: 'Guided Meditation Help',
      template: "This is a collection of recordings to guide you through your meditation practice.<br>"
              + "Downloaded meditations are shown in black; non-downloaded in gray<br>"
              + "Choose a meditation to download it. Select it again to listen.<br>"
              + "To remove a guided meditation, select (-) on the header bar and select"
              + " (-) next to the meditation.",
      buttons: [
        {
          text: 'Got it!',
          type: 'button-positive',
          onTap: function () {
            helptipState.setShown(gmHelptipKey);
          }
        }
      ]
    })
  }

  // events
  $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if (toState.name === 'tabs.guidedmeditationList') {
      if ( !helptipState.wasShown(gmHelptipKey) ) {
        showHelptip();
      }
      colorMeditations();
    }
  });

  $rootScope.$on('guidedmeditation:added', colorMeditations);
  $rootScope.$on('guidedmeditation:removed', colorMeditations);
})
