#!/usr/bin/env perl6
use v6;

class Source {
  has $.path;
  has $.code;
}

my @SOURCE := [
  Source.new(
    :path<platforms/ios/WorkwellNW/Classes/AppDelegate.m>,
    :code(q:to/APP_DELEGATE/)
    /*
     Licensed to the Apache Software Foundation (ASF) under one
     or more contributor license agreements.  See the NOTICE file
     distributed with this work for additional information
     regarding copyright ownership.  The ASF licenses this file
     to you under the Apache License, Version 2.0 (the
     "License"); you may not use this file except in compliance
     with the License.  You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing,
     software distributed under the License is distributed on an
     "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     KIND, either express or implied.  See the License for the
     specific language governing permissions and limitations
     under the License.
     */

    //
    //  AppDelegate.m
    //  WorkwellNW
    //
    //  Created by ___FULLUSERNAME___ on ___DATE___.
    //  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
    //

    #import "AppDelegate.h"
    #import "MainViewController.h"

    @implementation AppDelegate

    - (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
    {
        self.viewController = [[MainViewController alloc] init];
        [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
        return [super application:application didFinishLaunchingWithOptions:launchOptions];
    }

    @end
    APP_DELEGATE
  ),
];

for @SOURCE -> $source {
  given $source.path {
    when .IO.f { # topicalizing... just cuz i can :)
      unless $source.code eq .IO.slurp {
        .IO.unlink;
        spurt $_, $source.code;
      }
    }
    default { spurt $_, $source.code }
  }
}
